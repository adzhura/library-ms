package com.otus.edu.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MailService {
    private JavaMailSender mailSender;

    private String from;
    private String to;

    @Autowired
    public MailService(JavaMailSender mailSender,
            @Value("${spring.mail.username}") String from,
            @Value("${library.auth.defaultEmail}") String to) {
        this.mailSender = mailSender;
        this.from = from;
        this.to = to;
    }

    public void sendSecreteCode(String email, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(email.endsWith("@host.com") ? to : email);
        message.setSubject("Library");
        message.setText(code);
        mailSender.send(message);
        log.info("sendSecreteCode to {}: {}", email, code);
    }
}
