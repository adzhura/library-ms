package com.otus.edu.auth.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSender;

import com.otus.edu.auth.service.MailService;

@SpringBootConfiguration
@Import(MailSenderAutoConfiguration.class)
@ImportAutoConfiguration(classes = MailSenderAutoConfiguration.class)
public class TestConfig {
    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String from;

    @Value("${library.auth.defaultEmail}")
    private String to;

    @Bean
    public MailService mailService() {
        return new MailService(javaMailSender, from, to);
    }
}
