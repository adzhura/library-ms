package com.otus.edu.library.client.form.author.edit;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class AuthorEditViewImpl extends Composite implements AuthorEditView {
    private static GenreEditViewImplUiBinder uiBinder = GWT
            .create(GenreEditViewImplUiBinder.class);

    interface GenreEditViewImplUiBinder
            extends UiBinder<Widget, AuthorEditViewImpl> {
    }

    @UiField
    TextBox idField;
    @UiField
    TextBox firstNameField;
    @UiField
    TextBox lastNameField;
    @UiField
    Button saveButton;
    @UiField
    Button cancelButton;

    public AuthorEditViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @Override
    public HasValue<String> idDisplay() {
        return idField;
    }

    @Override
    public HasValue<String> firstNameDisplay() {
        return firstNameField;
    }

    @Override
    public HasValue<String> lastNameDisplay() {
        return lastNameField;
    }

    @Override
    public HasClickHandlers saveAction() {
        return saveButton;
    }

    @Override
    public HasEnabled saveEnabled() {
        return saveButton;
    }

    @Override
    public HasClickHandlers cancelAction() {
        return cancelButton;
    }
}