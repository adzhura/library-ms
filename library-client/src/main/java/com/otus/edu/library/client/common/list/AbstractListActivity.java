package com.otus.edu.library.client.common.list;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.Range;
import com.otus.edu.library.client.bean.HasId;
import com.otus.edu.library.client.bean.HasIdKeyProvider;

public abstract class AbstractListActivity<T extends HasId<?>>
        extends AbstractActivity {
    private List<T> data = new ArrayList<>();

    private ProvidesKey<T> keyProvider = new HasIdKeyProvider<>();
    private AsyncDataProvider<T> dataProvider;
    private String findText;

    protected void initView(ListView<T> view) {
        dataProvider = new AsyncDataProvider<T>(keyProvider) {
            @Override
            protected void onRangeChanged(HasData<T> display) {
                Range range = display.getVisibleRange();
                updateRowData(display, range.getStart(), data.stream()
                        .skip(range.getStart()).collect(Collectors.toList()));
            }
        };
        dataProvider.addDataDisplay(view.dataDisplay());

        loadData(r -> {
            data = r;
            updateDisplay();
        });

        view.findAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                findText = view.findText().getValue();
                updateDisplay();
            }
        });

        view.resetAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                view.findText().setValue(null);
                findText = view.findText().getValue();
                updateDisplay();
            }
        });
    }

    protected abstract void loadData(Consumer<List<T>> consumer);

    protected abstract boolean matchBean(String text, T bean);

    protected void doDelete(T bean) {
        data.remove(bean);
        updateDisplay();
    }

    private void updateDisplay() {
        List<T> dataList = null;

        if (findText != null && !findText.trim().isEmpty()) {
            dataList = data.stream().filter(o -> matchBean(findText, o))
                    .collect(Collectors.toList());
        } else {
            dataList = data;
        }

        dataProvider.updateRowCount(dataList.size(), true);
        dataProvider.updateRowData(0, dataList);
    }
}