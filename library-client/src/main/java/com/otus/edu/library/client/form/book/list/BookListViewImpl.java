package com.otus.edu.library.client.form.book.list;

import java.util.Optional;

import com.google.gwt.cell.client.AbstractSafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.IdentityColumn;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.otus.edu.library.client.bean.Book;
import com.otus.edu.library.client.common.list.AbstractListViewImpl;

public class BookListViewImpl extends AbstractListViewImpl<Book>
        implements BookListView {
    public BookListViewImpl() {
        super("Books");
    }

    @Override
    protected void initTable(DataGrid<Book> dataGrid) {
        Column<Book, String> idColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book object) {
                return object.getId();
            }
        };
        dataGrid.addColumn(idColumn, "id");
        dataGrid.setColumnWidth(idColumn, 50, Unit.PX);

        Column<Book, String> authorColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book object) {
                return object.getAuthor().getFirstName() + " "
                        + object.getAuthor().getLastName();
            }
        };
        dataGrid.addColumn(authorColumn, "author");
        dataGrid.setColumnWidth(authorColumn, 100, Unit.PX);

        Column<Book, String> genreColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book object) {
                return object.getGenre().getName();
            }
        };
        dataGrid.addColumn(genreColumn, "genre");
        dataGrid.setColumnWidth(genreColumn, 100, Unit.PX);

        Column<Book, String> nameColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book object) {
                return object.getName();
            }
        };
        dataGrid.addColumn(nameColumn, "title");
        dataGrid.setColumnWidth(nameColumn, 100, Unit.PX);

        SafeHtmlRenderer<Book> anchorRenderer = new AbstractSafeHtmlRenderer<Book>() {
            @Override
            public SafeHtml render(Book object) {
                String commentCount = Optional.ofNullable(object.getComments())
                        .map(l -> Integer.toString(l.size())).orElse("0");

                SafeHtmlBuilder sb = new SafeHtmlBuilder();

                sb.appendHtmlConstant("<a href=\"" + Window.Location.getPath()
                        + "#bookComments:" + object.getId() + "\">")
                        .appendEscaped(commentCount).appendHtmlConstant("</a>");
                return sb.toSafeHtml();
            }
        };
        Column<Book, Book> commentCountColumn = new IdentityColumn<Book>(
                new AbstractSafeHtmlCell<Book>(anchorRenderer) {
                    @Override
                    protected void render(Context context, SafeHtml data,
                            SafeHtmlBuilder sb) {
                        sb.append(data);
                    }
                }) {
        };
        dataGrid.addColumn(commentCountColumn, "comments");
        dataGrid.setColumnWidth(commentCountColumn, 60, Unit.PX);

        dataGrid.setWidth("750px");

        super.initTable(dataGrid);
    }
}