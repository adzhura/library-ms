package com.otus.edu.library.client.main;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.IsWidget;
import com.otus.edu.library.client.bean.Page;

public interface MainView extends IsWidget {
    void setPresenter(Presenter presenter);

    public interface Presenter {
        void goTo(Place place);
    }

    void addLink(Page page);
    
    HasClickHandlers logoutAction();
}
