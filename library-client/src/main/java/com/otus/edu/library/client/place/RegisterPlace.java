package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class RegisterPlace extends Place {
    public RegisterPlace() {
    }

    @Prefix(value = "register")
    public static class Tokenizer implements PlaceTokenizer<RegisterPlace> {
        @Override
        public String getToken(RegisterPlace place) {
            return "";
        }

        @Override
        public RegisterPlace getPlace(String token) {
            return new RegisterPlace();
        }
    }
}