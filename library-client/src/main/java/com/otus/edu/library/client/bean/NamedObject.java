package com.otus.edu.library.client.bean;

public interface NamedObject<T> extends HasId<T> {
    String getName();
}
