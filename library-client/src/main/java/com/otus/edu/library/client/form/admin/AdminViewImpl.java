package com.otus.edu.library.client.form.admin;

import java.util.function.Supplier;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.otus.edu.library.client.bean.Account;
import com.otus.edu.library.client.common.list.AbstractListViewImpl;

public class AdminViewImpl extends AbstractListViewImpl<Account>
        implements AdminView {
    public AdminViewImpl() {
        super("Accounts");
    }

    @Override
    protected void initTable(DataGrid<Account> dataGrid) {
        Column<Account, String> idColumn = new TextColumn<Account>() {
            @Override
            public String getValue(Account object) {
                return object.getId();
            }
        };
        dataGrid.addColumn(idColumn, "id");
        dataGrid.setColumnWidth(idColumn, 150, Unit.PX);

        Column<Account, String> nameColumn = new TextColumn<Account>() {
            @Override
            public String getValue(Account object) {
                return object.getName();
            }
        };
        dataGrid.addColumn(nameColumn, "name");
        dataGrid.setColumnWidth(nameColumn, 100, Unit.PX);

        Column<Account, String> emailColumn = new TextColumn<Account>() {
            @Override
            public String getValue(Account object) {
                return object.getEmail();
            }
        };
        dataGrid.addColumn(emailColumn, "email");
        dataGrid.setColumnWidth(emailColumn, 150, Unit.PX);

        initEditColumn(dataGrid, () -> "View");

        dataGrid.setWidth("700px");
    }

    @Override
    protected void initDeleteColumn(DataGrid<Account> dataGrid,
            Supplier<String> caption) {
    }
}