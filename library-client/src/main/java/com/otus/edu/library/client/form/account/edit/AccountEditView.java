package com.otus.edu.library.client.form.account.edit;

import com.google.gwt.user.client.ui.HasValue;
import com.otus.edu.library.client.common.edit.EditView;

public interface AccountEditView extends EditView {
    HasValue<String> idDisplay();

    HasValue<String> nameDisplay();

    HasValue<String> emailDisplay();
}