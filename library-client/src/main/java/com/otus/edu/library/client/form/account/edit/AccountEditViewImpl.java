package com.otus.edu.library.client.form.account.edit;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class AccountEditViewImpl extends Composite implements AccountEditView {
    private static AccountEditViewImplUiBinder uiBinder = GWT
            .create(AccountEditViewImplUiBinder.class);

    interface AccountEditViewImplUiBinder
            extends UiBinder<Widget, AccountEditViewImpl> {
    }

    @UiField
    TextBox idField;
    @UiField
    TextBox nameField;
    @UiField
    TextBox emailField;
    @UiField
    Button saveButton;
    @UiField
    Button cancelButton;
    
    public AccountEditViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @Override
    public HasValue<String> idDisplay() {
        return idField;
    }

    @Override
    public HasValue<String> nameDisplay() {
        return nameField;
    }
    
    @Override
    public HasValue<String> emailDisplay() {
        return emailField;
    }

    @Override
    public HasClickHandlers saveAction() {
        return saveButton;
    }
    
    @Override
    public HasClickHandlers cancelAction() {
        return cancelButton;
    }

    @Override
    public HasEnabled saveEnabled() {
        return saveButton;
    }
}