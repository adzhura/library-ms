package com.otus.edu.library.client.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

public interface HasPageClickHandlers extends HasHandlers {
    HandlerRegistration addPageClickHandler(PageClickHadler handler);
}
