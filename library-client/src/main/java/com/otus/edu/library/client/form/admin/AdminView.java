package com.otus.edu.library.client.form.admin;

import com.otus.edu.library.client.bean.Account;
import com.otus.edu.library.client.common.list.ListView;

public interface AdminView extends ListView<Account> {
}