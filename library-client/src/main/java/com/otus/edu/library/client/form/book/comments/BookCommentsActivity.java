package com.otus.edu.library.client.form.book.comments;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.Range;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Book;
import com.otus.edu.library.client.bean.Comment;
import com.otus.edu.library.client.bean.HasIdKeyProvider;
import com.otus.edu.library.client.place.BookCommentsPlace;
import com.otus.edu.library.client.service.BookService;
import com.otus.edu.library.client.service.CommentService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class BookCommentsActivity extends AbstractActivity {
    private ClientFactory clientFactory;
    private String id;

    private List<Comment> commentList = new ArrayList<>();

    private ProvidesKey<Comment> keyProvider = new HasIdKeyProvider<>();
    private AsyncDataProvider<Comment> dataProvider;
    private String findText;

    public BookCommentsActivity(BookCommentsPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        BookCommentsView view = clientFactory.getBookCommentsView();
        containerWidget.setWidget(view.asWidget());

        dataProvider = new AsyncDataProvider<Comment>(keyProvider) {
            @Override
            protected void onRangeChanged(HasData<Comment> display) {
                Range range = display.getVisibleRange();
                updateRowData(display, range.getStart(), commentList.stream()
                        .skip(range.getStart()).collect(Collectors.toList()));
            }
        };

        dataProvider.addDataDisplay(view.commentDisplay());

        view.authorDisplay().setText(null);
        view.genreDisplay().setText(null);
        view.nameDisplay().setText("");
        commentList = new ArrayList<>();

        if (id != null && !id.trim().isEmpty()) {
            updateDisplay(view);
        } else {
            updateCommentDisplay();
        }

        view.findAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                findText = view.findText().getValue();
                updateCommentDisplay();
            }
        });

        view.resetAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                view.findText().setValue(null);
                findText = view.findText().getValue();
                updateCommentDisplay();
            }
        });
        view.addCommentAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                REST.call(CommentService.getInstance()).addComment(id,
                        new Comment(0, view.userNameDisplay().getValue(),
                                view.textDisplay().getValue()),
                        RestCallback.create(r -> {
                            view.userNameDisplay().setValue(null);
                            view.textDisplay().setValue(null);

                            updateDisplay(view);
                        }));
            }
        });
    }

    private void updateDisplay(BookCommentsView view) {
        loadData(id, book -> {

            if (book != null) {
                view.authorDisplay().setText(book.getAuthor().getFirstName()
                        + " " + book.getAuthor().getLastName());
                view.genreDisplay().setText(book.getGenre().getName());
                view.nameDisplay().setText(book.getName());
                commentList = Optional.ofNullable(book.getComments())
                        .orElse(new ArrayList<>());

                updateCommentDisplay();
            }
        });
    }

    private void loadData(String id, Consumer<Book> consumer) {
        REST.call(BookService.getInstance()).findOne(id,
                RestCallback.create(consumer));
    }

    private void updateCommentDisplay() {
        List<Comment> dataList = null;

        if (findText != null && !findText.trim().isEmpty()) {
            dataList = commentList.stream()
                    .filter(o -> o.getUserName().contains(findText)
                            || o.getText().contains(findText))
                    .collect(Collectors.toList());
        } else {
            dataList = commentList;
        }

        dataProvider.updateRowCount(dataList.size(), true);
        dataProvider.updateRowData(0, dataList);
    }
}