package com.otus.edu.library.client.form.book.comments;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;
import com.otus.edu.library.client.bean.Comment;
import com.otus.edu.library.client.bean.HasIdKeyProvider;

public class BookCommentsViewImpl extends Composite
        implements BookCommentsView {
    private static GenreEditViewImplUiBinder uiBinder = GWT
            .create(GenreEditViewImplUiBinder.class);

    interface GenreEditViewImplUiBinder
            extends UiBinder<Widget, BookCommentsViewImpl> {
    }

    @UiField
    Label authorField;
    @UiField
    Label genreField;
    @UiField
    Label nameField;

    @UiField
    TextBox userNameField;
    @UiField
    TextArea textField;
    @UiField
    Button addCommentButton;

    @UiField
    TextBox findText;
    @UiField
    Button findButton;
    @UiField
    Button resetButton;

    @UiField
    SimplePanel tableContainer;

    private DataGrid<Comment> dataGrid;

    public BookCommentsViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));

        ProvidesKey<Comment> keyProvider = new HasIdKeyProvider<Comment>();
        dataGrid = new DataGrid<>(keyProvider);

        dataGrid.setSize("600px", "300px");

        Column<Comment, String> userNameColumn = new TextColumn<Comment>() {
            @Override
            public String getValue(Comment object) {
                return object.getUserName();
            }
        };
        dataGrid.addColumn(userNameColumn, "User name");
        dataGrid.setColumnWidth(userNameColumn, 100, Unit.PX);

        Column<Comment, String> textColumn = new TextColumn<Comment>() {
            @Override
            public String getValue(Comment object) {
                return object.getText();
            }
        };
        dataGrid.addColumn(textColumn, "Text");
        dataGrid.setColumnWidth(textColumn, 200, Unit.PX);

        tableContainer.setWidget(dataGrid);
    }

    @Override
    public HasText nameDisplay() {
        return nameField;
    }

    @Override
    public HasText authorDisplay() {
        return authorField;
    }

    @Override
    public HasText genreDisplay() {
        return genreField;
    }

    @Override
    public HasValue<String> userNameDisplay() {
        return userNameField;
    }

    @Override
    public HasValue<String> textDisplay() {
        return textField;
    }

    @Override
    public HasClickHandlers addCommentAction() {
        return addCommentButton;
    }

    @Override
    public HasValue<String> findText() {
        return findText;
    }

    @Override
    public HasClickHandlers findAction() {
        return findButton;
    }

    @Override
    public HasClickHandlers resetAction() {
        return resetButton;
    }

    @Override
    public HasData<Comment> commentDisplay() {
        return dataGrid;
    }
}