package com.otus.edu.library.client.form.register;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class RegisterViewImpl extends Composite implements RegisterView {
    private static RegisterViewImplUiBinder uiBinder = GWT
            .create(RegisterViewImplUiBinder.class);

    interface RegisterViewImplUiBinder
            extends UiBinder<Widget, RegisterViewImpl> {
    }

    @UiField
    TextBox userField;

    @UiField
    Label passwordLabel;
    @UiField
    PasswordTextBox passwordField;

    @UiField
    Label passwordConfirmLabel;
    @UiField
    PasswordTextBox passwordConfirmField;

    @UiField
    Button registerButton;

    public RegisterViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
        userField.getElement().setPropertyString("placeholder", "user@host.com");
        registerButton.setEnabled(false);
    }

    @Override
    public void setPresenter(Presenter presenter) {
    }

    @Override
    public HasClickHandlers registerAction() {
        return registerButton;
    }

    @Override
    public HasValue<String> userField() {
        return userField;
    }

    @Override
    public HasValue<String> passwordField() {
        return passwordField;
    }

    @Override
    public HasValue<String> passwordConfirmField() {
        return passwordConfirmField;
    }

}