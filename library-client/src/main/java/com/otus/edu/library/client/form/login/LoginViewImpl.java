package com.otus.edu.library.client.form.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginViewImpl extends Composite implements LoginView {
    private static LoginViewImplUiBinder uiBinder = GWT
            .create(LoginViewImplUiBinder.class);

    interface LoginViewImplUiBinder extends UiBinder<Widget, LoginViewImpl> {
    }

    @UiField
    TextBox userField;

    @UiField
    Label passwordLabel;
    @UiField
    PasswordTextBox passwordField;

    @UiField
    Label codeLabel;
    @UiField
    TextBox codeField;

    @UiField
    Button loginButton;

    @UiField
    Button confirmButton;

    @UiField
    Anchor registerLink;

    public LoginViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
        loginButton.setEnabled(false);
        setMode(Mode.LOGIN);
    }

    @Override
    public void setPresenter(Presenter presenter) {
    }

    @Override
    public HasClickHandlers loginAction() {
        return loginButton;
    }

    @Override
    public HasClickHandlers confirmAction() {
        return confirmButton;
    }

    @Override
    public HasClickHandlers registerAction() {
        return registerLink;
    }

    @Override
    public HasValue<String> userField() {
        return userField;
    }

    @Override
    public HasValue<String> passwordField() {
        return passwordField;
    }

    @Override
    public HasValue<String> codeField() {
        return codeField;
    }

    @Override
    public void setMode(Mode mode) {
        passwordLabel.setVisible(mode == Mode.LOGIN);
        passwordField.setVisible(mode == Mode.LOGIN);
        codeLabel.setVisible(mode == Mode.CONFIRM);
        codeField.setVisible(mode == Mode.CONFIRM);
        loginButton.setVisible(mode == Mode.LOGIN);
        confirmButton.setVisible(mode == Mode.CONFIRM);
        registerLink.setVisible(mode == Mode.LOGIN);
    }
}