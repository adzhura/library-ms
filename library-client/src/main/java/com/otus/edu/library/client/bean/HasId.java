package com.otus.edu.library.client.bean;

public interface HasId<T> {
    T getId();

    void setId(T id);
}
