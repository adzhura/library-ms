package com.otus.edu.library.client.common.edit;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.IsWidget;

public interface EditView extends IsWidget {
    HasClickHandlers saveAction();

    HasClickHandlers cancelAction();

    HasEnabled saveEnabled();
}