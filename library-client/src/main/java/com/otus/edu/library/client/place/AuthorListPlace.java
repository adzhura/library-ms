package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class AuthorListPlace extends Place {
    public static final String TOKEN = "authors";

    public AuthorListPlace() {
    }

    @Prefix(TOKEN)
    public static class Tokenizer implements PlaceTokenizer<AuthorListPlace> {
        @Override
        public String getToken(AuthorListPlace place) {
            return "";
        }

        @Override
        public AuthorListPlace getPlace(String token) {
            return new AuthorListPlace();
        }
    }
}