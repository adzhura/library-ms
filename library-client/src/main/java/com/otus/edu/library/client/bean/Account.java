package com.otus.edu.library.client.bean;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Account implements HasId<String> {
    private String id;
    private String name;
    private String email;
    private List<String> roles;
}
