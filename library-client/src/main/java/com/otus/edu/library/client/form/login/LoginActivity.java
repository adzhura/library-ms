package com.otus.edu.library.client.form.login;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.LoginInfo;
import com.otus.edu.library.client.form.login.LoginView.Mode;
import com.otus.edu.library.client.place.LoginPlace;
import com.otus.edu.library.client.place.RegisterPlace;
import com.otus.edu.library.client.service.LoginService;
import com.otus.edu.library.client.service.RestCallback;

public class LoginActivity extends AbstractActivity
        implements LoginView.Presenter {
    private ClientFactory clientFactory;
    private String authorizationToken;

    public LoginActivity(LoginPlace place, ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        LoginView view = clientFactory.getLoginView();
        containerWidget.setWidget(view.asWidget());

        ValueChangeHandler<String> handler = new LoginInfoChangeHandler(view);
        view.userField().addValueChangeHandler(handler);
        view.passwordField().addValueChangeHandler(handler);

        view.loginAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                invokeLogin(view);
            }
        });

        view.confirmAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                invokeConfirm(view);
            }
        });

        view.registerAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clientFactory.getPlaceController().goTo(new RegisterPlace());
            }
        });
    }

    private void invokeLogin(LoginView view) {
        LoginService.getInstance()
                .login(new LoginInfo(view.userField().getValue(),
                        view.passwordField().getValue()),
                        RestCallback.create((method, r) -> {
                            authorizationToken = method.getResponse()
                                    .getHeader("Authorization");
//                            Window.alert(
//                                    "Authorization: " + authorizationToken);
                            view.setMode(Mode.CONFIRM);
                        }));
    }

    private void invokeConfirm(LoginView view) {
        LoginService.getInstance().confirm(authorizationToken,
                view.codeField().getValue(),
                RestCallback.create((method, r) -> {
                    String token = method.getResponse()
                            .getHeader("Authorization");
                    Cookies.setCookie("Authorization", token);
                    // Window.alert(Window.Location.getHref());

                    String href = Window.Location.getHref();
                    int loc = href.indexOf('#');

                    if (loc > 0) {
                        href = href.substring(0, loc);
                    }
                    Window.Location.assign(href);
                }));
    }

    @Override
    public void goTo(Place place) {
        clientFactory.getPlaceController().goTo(place);
    }

    private static final class LoginInfoChangeHandler
            implements ValueChangeHandler<String> {
        private LoginView view;

        public LoginInfoChangeHandler(LoginView view) {
            this.view = view;
        }

        @Override
        public void onValueChange(ValueChangeEvent<String> event) {
            ((HasEnabled) view.loginAction())
                    .setEnabled(fieldHasValue(view.userField())
                            && fieldHasValue(view.passwordField()));
        }

        private boolean fieldHasValue(HasValue<String> field) {
            return field.getValue() != null
                    && field.getValue().trim().length() > 0;
        }
    }
}