package com.otus.edu.library.client.service;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;
import com.otus.edu.library.client.bean.Author;

@Path("/authors")
public interface AuthorService extends RestService {
    public static AuthorService getInstance() {
        Resource resource = new Resource(ServiceConfig.libraryUrl + "/authors",
                new HashMap<>());

        AuthorService service = GWT.create(AuthorService.class);
        ((RestServiceProxy) service).setResource(resource);

        return service;
    }

    @GET()
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public void findAll(MethodCallback<List<Author>> callback);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void findOne(@PathParam("id") String id,
            MethodCallback<Author> callback);

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Author author, MethodCallback<Integer> callback);

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("id") String id, Author author,
            MethodCallback<Void> callback);

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id,
            MethodCallback<Void> callback);
}
