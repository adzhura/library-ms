package com.otus.edu.library.client.bean;

import java.util.List;

public class Book implements NamedObject<String> {
    private String id;

    private String name;

    private Author author;

    private Genre genre;

    private List<Comment> comments;

    public Book() {
    }

    public Book(String id, String name, Author author, Genre genre,
            List<Comment> comments) {
        super();
        this.id = id;
        this.name = name;
        this.author = author;
        this.genre = genre;
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
