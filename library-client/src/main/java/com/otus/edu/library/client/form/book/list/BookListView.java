package com.otus.edu.library.client.form.book.list;

import com.otus.edu.library.client.bean.Book;
import com.otus.edu.library.client.common.list.ListView;

public interface BookListView extends ListView<Book> {
}