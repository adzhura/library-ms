package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class AuthorEditPlace extends Place {
    private String id = "";

    public AuthorEditPlace() {
    }

    public AuthorEditPlace(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Prefix("authorEdit")
    public static class Tokenizer implements PlaceTokenizer<AuthorEditPlace> {
        @Override
        public String getToken(AuthorEditPlace place) {
            return place.getId();
        }

        @Override
        public AuthorEditPlace getPlace(String token) {
            return new AuthorEditPlace(token);
        }
    }
}