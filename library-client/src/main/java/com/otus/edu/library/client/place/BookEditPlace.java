package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class BookEditPlace extends Place {
    private String id = "";

    public BookEditPlace() {
    }

    public BookEditPlace(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Prefix("bookEdit")
    public static class Tokenizer implements PlaceTokenizer<BookEditPlace> {
        @Override
        public String getToken(BookEditPlace place) {
            return place.getId();
        }

        @Override
        public BookEditPlace getPlace(String token) {
            return new BookEditPlace(token);
        }
    }
}