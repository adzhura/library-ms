package com.otus.edu.library.client.bean;

public class Page {
    private String name;
    private String token;

    public Page() {
    }

    public Page(String name, String token) {
        super();
        this.name = name;
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
