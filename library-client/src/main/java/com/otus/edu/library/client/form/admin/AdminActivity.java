package com.otus.edu.library.client.form.admin;

import java.util.List;
import java.util.function.Consumer;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.HasVisibility;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Account;
import com.otus.edu.library.client.common.list.AbstractListActivity;
import com.otus.edu.library.client.place.AccountEditPlace;
import com.otus.edu.library.client.place.AdminPlace;
import com.otus.edu.library.client.service.AccountService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class AdminActivity extends AbstractListActivity<Account> {
    private ClientFactory clientFactory;

    public AdminActivity(AdminPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        AdminView view = clientFactory.getAdminView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                    .goTo(new AccountEditPlace(t.getId()));
        });
        ((HasVisibility)view.createAction()).setVisible(false);
    }

    @Override
    protected void doDelete(Account account) {
    }

    @Override
    protected void loadData(Consumer<List<Account>> consumer) {
        REST.call(AccountService.getInstance())
                .findAll(RestCallback.create(consumer));
    }

    @Override
    protected boolean matchBean(String text, Account bean) {
        return bean.getId().toString().contains(text)
                || bean.getName().toString().contains(text);
    }
}