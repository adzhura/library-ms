package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class BookCommentsPlace extends Place {
    private String id = "";

    public BookCommentsPlace(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Prefix("bookComments")
    public static class Tokenizer implements PlaceTokenizer<BookCommentsPlace> {
        @Override
        public String getToken(BookCommentsPlace place) {
            return place.getId();
        }

        @Override
        public BookCommentsPlace getPlace(String token) {
            return new BookCommentsPlace(token);
        }
    }
}