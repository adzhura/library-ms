package com.otus.edu.library.client.form.author.list;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.common.list.AbstractListViewImpl;

public class AuthorListViewImpl extends AbstractListViewImpl<Author>
        implements AuthorListView {
    public AuthorListViewImpl() {
        super("Authors");
    }

    @Override
    protected void initTable(DataGrid<Author> dataGrid) {
        Column<Author, String> idColumn = new TextColumn<Author>() {
            @Override
            public String getValue(Author object) {
                return object.getId();
            }
        };
        dataGrid.addColumn(idColumn, "id");
        dataGrid.setColumnWidth(idColumn, 50, Unit.PX);

        Column<Author, String> firstNameColumn = new TextColumn<Author>() {
            @Override
            public String getValue(Author object) {
                return object.getFirstName();
            }
        };
        dataGrid.addColumn(firstNameColumn, "first name");
        dataGrid.setColumnWidth(firstNameColumn, 100, Unit.PX);

        Column<Author, String> lastNameColumn = new TextColumn<Author>() {
            @Override
            public String getValue(Author object) {
                return object.getLastName();
            }
        };
        dataGrid.addColumn(lastNameColumn, "last name");
        dataGrid.setColumnWidth(lastNameColumn, 100, Unit.PX);
        
        super.initTable(dataGrid);
    }
}