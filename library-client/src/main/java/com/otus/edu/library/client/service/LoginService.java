package com.otus.edu.library.client.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;
import com.otus.edu.library.client.bean.LoginInfo;

@Path("/auth")
public interface LoginService extends RestService {
    public static LoginService getInstance() {
        Resource resource = new Resource(ServiceConfig.authUrl);

        LoginService service = GWT.create(LoginService.class);
        ((RestServiceProxy) service).setResource(resource);

        return service;
    }

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void login(LoginInfo loginInfo, MethodCallback<Object> callback);

    @POST
    @Path("/confirm")
    @Produces(MediaType.APPLICATION_JSON)
    public void confirm(@HeaderParam("Authorization") String token,
            @FormParam("code") String code, MethodCallback<Object> callback);
}
