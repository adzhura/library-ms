package com.otus.edu.library.client.common.edit;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;

public abstract class AbstractEditActivity extends AbstractActivity {

    protected void initView(EditView view) {
        view.cancelAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                History.back();
            }
        });
    }
}
