package com.otus.edu.library.client.form.genre.edit;

import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.common.edit.AbstractEditActivity;
import com.otus.edu.library.client.place.GenreEditPlace;
import com.otus.edu.library.client.place.GenreListPlace;
import com.otus.edu.library.client.service.GenreService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class GenreEditActivity extends AbstractEditActivity {
    private ClientFactory clientFactory;
    private String id;

    public GenreEditActivity(GenreEditPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        GenreEditView view = clientFactory.getGenreEditView();
        containerWidget.setWidget(view.asWidget());
        
        initView(view);

        view.idDisplay().setValue(null);
        view.nameDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty()) {
            loadData(id, genre -> {
                view.saveEnabled().setEnabled(genre != null);

                if (genre != null) {
                    view.idDisplay().setValue(genre.getId());
                    view.nameDisplay().setValue(genre.getName());
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String id = view.idDisplay().getValue();
                id = "".equals(id) ? null : id;

                Genre genre = new Genre(id, view.nameDisplay().getValue());

                saveData(genre, r -> clientFactory.getPlaceController()
                        .goTo(new GenreListPlace()));
            }
        });
    }

    private void loadData(String id, Consumer<Genre> consumer) {
        REST.call(GenreService.getInstance()).findOne(id, RestCallback.create(consumer));
    }

    private void saveData(Genre genre, Consumer<Void> consumer) {
        GenreService service = REST.call(GenreService.getInstance());

        if (genre.getId() != null) {
            service.update(id.toString(), genre, RestCallback.create(consumer));
        } else {
            service.create(genre,
                    RestCallback.create(r -> consumer.accept(null)));
        }
    }
}