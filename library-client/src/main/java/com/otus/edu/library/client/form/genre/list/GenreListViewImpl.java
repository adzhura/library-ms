package com.otus.edu.library.client.form.genre.list;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.common.list.AbstractListViewImpl;

public class GenreListViewImpl extends AbstractListViewImpl<Genre>
        implements GenreListView {
    public GenreListViewImpl() {
        super("Genres");
    }

    @Override
    protected void initTable(DataGrid<Genre> dataGrid) {
        Column<Genre, String> idColumn = new TextColumn<Genre>() {
            @Override
            public String getValue(Genre object) {
                return object.getId();
            }
        };
        dataGrid.addColumn(idColumn, "id");
        dataGrid.setColumnWidth(idColumn, 50, Unit.PX);

        Column<Genre, String> nameColumn = new TextColumn<Genre>() {
            @Override
            public String getValue(Genre object) {
                return object.getName();
            }
        };
        dataGrid.addColumn(nameColumn, "name");
        dataGrid.setColumnWidth(nameColumn, 100, Unit.PX);
        
        super.initTable(dataGrid);
    }
}