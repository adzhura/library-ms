package com.otus.edu.library.client.form.genre.edit;

import com.google.gwt.user.client.ui.HasValue;
import com.otus.edu.library.client.common.edit.EditView;

public interface GenreEditView extends EditView {
    HasValue<String> idDisplay();

    HasValue<String> nameDisplay();
}