package com.otus.edu.library.client.form.book.edit;

import com.google.gwt.user.client.ui.HasConstrainedValue;
import com.google.gwt.user.client.ui.HasValue;
import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.common.edit.EditView;

public interface BookEditView extends EditView {
    HasValue<String> idDisplay();

    HasValue<String> nameDisplay();

    HasConstrainedValue<Author> authorDisplay();

    HasConstrainedValue<Genre> genreDisplay();
}