package com.otus.edu.library.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface PageClickHadler extends EventHandler {
    void onPageClick(PageClickEvent e);
}
