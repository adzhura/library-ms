package com.otus.edu.library.client.bean;

public class Comment implements HasId<Integer> {
    private Integer id;

    private String userName;

    private String text;

    public Comment() {
    }

    public Comment(Integer id, String userName, String text) {
        super();
        this.id = id;
        this.userName = userName;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
