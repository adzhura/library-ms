package com.otus.edu.library.client.common.list;

import java.util.function.Consumer;
import java.util.function.Supplier;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.IdentityColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;
import com.otus.edu.library.client.bean.HasId;
import com.otus.edu.library.client.bean.HasIdKeyProvider;

public abstract class AbstractListViewImpl<T extends HasId<String>>
        extends Composite implements ListView<T> {
    private static GenreListViewImplUiBinder uiBinder = GWT
            .create(GenreListViewImplUiBinder.class);

    interface GenreListViewImplUiBinder
            extends UiBinder<Widget, AbstractListViewImpl<?>> {
    }

    @UiField
    HeadingElement titleHtml;

    @UiField
    TextBox findText;
    @UiField
    Button findButton;
    @UiField
    Button resetButton;
    @UiField
    Button createButton;
    @UiField
    SimplePanel tableContainer;

    private DataGrid<T> dataGrid;

    private Consumer<T> editHandler;
    private Consumer<T> deleteHandler;

    public AbstractListViewImpl(String title) {
        initWidget(uiBinder.createAndBindUi(this));

        titleHtml.setInnerText(title);

        ProvidesKey<T> keyProvider = new HasIdKeyProvider<T>();
        dataGrid = new DataGrid<>(keyProvider);

        dataGrid.setSize("600px", "300px");
        initTable(dataGrid);

        tableContainer.setWidget(dataGrid);
    }

    protected void initTable(DataGrid<T> dataGrid)
    {
        initEditColumn(dataGrid, () -> "Edit");
        initDeleteColumn(dataGrid, () -> "Delete");
    }

    protected void initDeleteColumn(DataGrid<T> dataGrid, Supplier<String> caption) {
        Column<T, T> deleteColumn = new IdentityColumn<T>(
                new ActionCell<T>(caption.get(), new Delegate<T>() {
                    @Override
                    public void execute(T object) {
                        if (deleteHandler != null
                                && Window.confirm("Are you shure?")) {
                            deleteHandler.accept(object);
                        }
                    }
                }));
        dataGrid.addColumn(deleteColumn, "X");
        dataGrid.setColumnWidth(deleteColumn, 50, Unit.PX);
    }

    protected void initEditColumn(DataGrid<T> dataGrid, Supplier<String> caption) {
        Column<T, T> editColumn = new IdentityColumn<T>(
                new ActionCell<T>(caption.get(), new Delegate<T>() {
                    @Override
                    public void execute(T object) {
                        if (editHandler != null) {
                            editHandler.accept(object);
                        }
                    }
                }));
        dataGrid.addColumn(editColumn, "*");
        dataGrid.setColumnWidth(editColumn, 50, Unit.PX);
    }

    @Override
    public HasData<T> dataDisplay() {
        return dataGrid;
    }

    @Override
    public HasValue<String> findText() {
        return findText;
    }

    @Override
    public HasClickHandlers findAction() {
        return findButton;
    }

    @Override
    public HasClickHandlers resetAction() {
        return resetButton;
    }

    @Override
    public HasClickHandlers createAction() {
        return createButton;
    }

    @Override
    public void setEditHandler(Consumer<T> consumer) {
        this.editHandler = consumer;
    }

    @Override
    public void setDeleteHandler(Consumer<T> consumer) {
        this.deleteHandler = consumer;
    }
}