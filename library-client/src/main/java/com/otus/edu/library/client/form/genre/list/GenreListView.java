package com.otus.edu.library.client.form.genre.list;

import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.common.list.ListView;

public interface GenreListView extends ListView<Genre> {
}