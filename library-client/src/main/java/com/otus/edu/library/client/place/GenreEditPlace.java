package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class GenreEditPlace extends Place {
    private String id = "";

    public GenreEditPlace() {
    }

    public GenreEditPlace(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Prefix("genreEdit")
    public static class Tokenizer implements PlaceTokenizer<GenreEditPlace> {
        @Override
        public String getToken(GenreEditPlace place) {
            return place.getId();
        }

        @Override
        public GenreEditPlace getPlace(String token) {
            return new GenreEditPlace(token);
        }
    }
}