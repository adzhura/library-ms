package com.otus.edu.library.client.bean;

import java.util.Optional;

import com.google.gwt.view.client.ProvidesKey;

public class HasIdKeyProvider<T extends HasId<?>> implements ProvidesKey<T> {
    @Override
    public Object getKey(T item) {
        return Optional.ofNullable(item).map(i -> i.getId()).orElse(null);
    }
}
