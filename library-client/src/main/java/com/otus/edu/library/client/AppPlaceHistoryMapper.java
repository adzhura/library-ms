package com.otus.edu.library.client;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.otus.edu.library.client.place.AccountEditPlace;
import com.otus.edu.library.client.place.AdminPlace;
import com.otus.edu.library.client.place.AuthorEditPlace;
import com.otus.edu.library.client.place.AuthorListPlace;
import com.otus.edu.library.client.place.BookCommentsPlace;
import com.otus.edu.library.client.place.BookEditPlace;
import com.otus.edu.library.client.place.BookListPlace;
import com.otus.edu.library.client.place.GenreEditPlace;
import com.otus.edu.library.client.place.GenreListPlace;
import com.otus.edu.library.client.place.MainPlace;
import com.otus.edu.library.client.place.LoginPlace;
import com.otus.edu.library.client.place.RegisterPlace;

@WithTokenizers({ LoginPlace.Tokenizer.class, RegisterPlace.Tokenizer.class,
        MainPlace.Tokenizer.class, GenreListPlace.Tokenizer.class,
        GenreEditPlace.Tokenizer.class, AuthorListPlace.Tokenizer.class,
        AuthorEditPlace.Tokenizer.class, BookListPlace.Tokenizer.class,
        BookEditPlace.Tokenizer.class, BookCommentsPlace.Tokenizer.class,
        AdminPlace.Tokenizer.class, AccountEditPlace.Tokenizer.class })
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}