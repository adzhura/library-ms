package com.otus.edu.library.client.form.book.edit;

import java.util.Optional;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasConstrainedValue;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.Widget;
import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.bean.HasIdKeyProvider;

public class BookEditViewImpl extends Composite implements BookEditView {
    private static GenreEditViewImplUiBinder uiBinder = GWT
            .create(GenreEditViewImplUiBinder.class);

    interface GenreEditViewImplUiBinder
            extends UiBinder<Widget, BookEditViewImpl> {
    }

    @UiField
    TextBox idField;
    @UiField(provided = true)
    ValueListBox<Author> authorField;
    @UiField(provided = true)
    ValueListBox<Genre> genreField;
    @UiField
    TextBox nameField;
    @UiField
    Button saveButton;
    @UiField
    Button cancelButton;

    public BookEditViewImpl() {
        authorField = new ValueListBox<>(new AbstractRenderer<Author>() {
            @Override
            public String render(Author object) {
                return Optional.ofNullable(object)
                        .map(o -> o.getFirstName() + " " + o.getLastName())
                        .orElse("");
            }
        }, new HasIdKeyProvider<>());

        genreField = new ValueListBox<>(new AbstractRenderer<Genre>() {
            @Override
            public String render(Genre object) {
                return Optional.ofNullable(object).map(o -> o.getName())
                        .orElse("");
            }
        }, new HasIdKeyProvider<>());

        initWidget(uiBinder.createAndBindUi(this));
    }

    @Override
    public HasValue<String> idDisplay() {
        return idField;
    }

    @Override
    public HasValue<String> nameDisplay() {
        return nameField;
    }

    @Override
    public HasConstrainedValue<Author> authorDisplay() {
        return authorField;
    }

    @Override
    public HasConstrainedValue<Genre> genreDisplay() {
        return genreField;
    }

    @Override
    public HasClickHandlers saveAction() {
        return saveButton;
    }

    @Override
    public HasClickHandlers cancelAction() {
        return cancelButton;
    }

    @Override
    public HasEnabled saveEnabled() {
        return saveButton;
    }
}