package com.otus.edu.library.client.form.genre.edit;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class GenreEditViewImpl extends Composite implements GenreEditView {
    private static GenreEditViewImplUiBinder uiBinder = GWT
            .create(GenreEditViewImplUiBinder.class);

    interface GenreEditViewImplUiBinder
            extends UiBinder<Widget, GenreEditViewImpl> {
    }

    @UiField
    TextBox idField;
    @UiField
    TextBox nameField;
    @UiField
    Button saveButton;
    @UiField
    Button cancelButton;
    
    public GenreEditViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @Override
    public HasValue<String> idDisplay() {
        return idField;
    }

    @Override
    public HasValue<String> nameDisplay() {
        return nameField;
    }

    @Override
    public HasClickHandlers saveAction() {
        return saveButton;
    }
    
    @Override
    public HasClickHandlers cancelAction() {
        return cancelButton;
    }

    @Override
    public HasEnabled saveEnabled() {
        return saveButton;
    }
}