package com.otus.edu.library.client.form.author.edit;

import com.google.gwt.user.client.ui.HasValue;
import com.otus.edu.library.client.common.edit.EditView;

public interface AuthorEditView extends EditView {
    HasValue<String> idDisplay();

    HasValue<String> firstNameDisplay();

    HasValue<String> lastNameDisplay();
}