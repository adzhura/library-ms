package com.otus.edu.library.client.form.register;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.LoginInfo;
import com.otus.edu.library.client.place.LoginPlace;
import com.otus.edu.library.client.place.RegisterPlace;
import com.otus.edu.library.client.service.AccountService;
import com.otus.edu.library.client.service.RestCallback;

public class RegisterActivity extends AbstractActivity
        implements RegisterView.Presenter {

    private ClientFactory clientFactory;

    public RegisterActivity(RegisterPlace place, ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        RegisterView view = clientFactory.getRegisterView();
        containerWidget.setWidget(view.asWidget());

        ValueChangeHandler<String> handler = new LoginInfoChangeHandler(view);
        view.userField().addValueChangeHandler(handler);
        view.passwordField().addValueChangeHandler(handler);
        view.passwordConfirmField().addValueChangeHandler(handler);

        view.registerAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                invokeRegister(view);
            }
        });
    }

    private void invokeRegister(RegisterView view) {
        AccountService.getInstance().register(
                new LoginInfo(view.userField().getValue(),
                        view.passwordField().getValue()),
                RestCallback.create(r -> {
                    if (r.isSuccess()) {
                        Window.alert("Account created");
                        clientFactory.getPlaceController()
                                .goTo(new LoginPlace());
                    } else {
                        Window.alert("Error: " + r.getMessage());
                    }
                }));
    }

    @Override
    public void goTo(Place place) {
        clientFactory.getPlaceController().goTo(place);
    }

    private static final class LoginInfoChangeHandler
            implements ValueChangeHandler<String> {
        private RegisterView view;

        public LoginInfoChangeHandler(RegisterView view) {
            this.view = view;
        }

        @Override
        public void onValueChange(ValueChangeEvent<String> event) {
            ((HasEnabled) view.registerAction())
                    .setEnabled(fieldHasValue(view.userField())
                            && fieldHasValue(view.passwordField())
                            && view.passwordField().getValue().equals(
                                    view.passwordConfirmField().getValue()));
        }

        private boolean fieldHasValue(HasValue<String> field) {
            return field.getValue() != null
                    && field.getValue().trim().length() > 0;
        }
    }
}