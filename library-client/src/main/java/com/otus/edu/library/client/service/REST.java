package com.otus.edu.library.client.service;

import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.user.client.Cookies;

public class REST {
    public static <R extends RestService> R call(R service) {
        
        if (service instanceof RestServiceProxy) {
            Resource resource = ((RestServiceProxy) service).getResource();
            String cookie = Cookies.getCookie("Authorization");
            
            if (cookie != null && resource.getHeaders() != null) {
                resource.getHeaders().put("Authorization", cookie);
            }
        }

        return service;
    }
}
