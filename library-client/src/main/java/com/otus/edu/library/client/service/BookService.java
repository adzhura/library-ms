package com.otus.edu.library.client.service;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;
import com.otus.edu.library.client.bean.Book;

@Path("/books")
public interface BookService extends RestService {
    public static BookService getInstance() {
        Resource resource = new Resource(ServiceConfig.libraryUrl + "/books",
                new HashMap<>());

        BookService service = GWT.create(BookService.class);
        ((RestServiceProxy) service).setResource(resource);

        return service;
    }

    @GET()
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public void findAll(MethodCallback<List<Book>> callback);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void findOne(@PathParam("id") String id,
            MethodCallback<Book> callback);

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Book book, MethodCallback<Integer> callback);

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("id") String id, Book book,
            MethodCallback<Void> callback);

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id,
            MethodCallback<Void> callback);
}
