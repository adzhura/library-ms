package com.otus.edu.library.client;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.otus.edu.library.client.form.account.edit.AccountEditView;
import com.otus.edu.library.client.form.account.edit.AccountEditViewImpl;
import com.otus.edu.library.client.form.admin.AdminView;
import com.otus.edu.library.client.form.admin.AdminViewImpl;
import com.otus.edu.library.client.form.author.edit.AuthorEditView;
import com.otus.edu.library.client.form.author.edit.AuthorEditViewImpl;
import com.otus.edu.library.client.form.author.list.AuthorListView;
import com.otus.edu.library.client.form.author.list.AuthorListViewImpl;
import com.otus.edu.library.client.form.book.comments.BookCommentsView;
import com.otus.edu.library.client.form.book.comments.BookCommentsViewImpl;
import com.otus.edu.library.client.form.book.edit.BookEditView;
import com.otus.edu.library.client.form.book.edit.BookEditViewImpl;
import com.otus.edu.library.client.form.book.list.BookListView;
import com.otus.edu.library.client.form.book.list.BookListViewImpl;
import com.otus.edu.library.client.form.genre.edit.GenreEditView;
import com.otus.edu.library.client.form.genre.edit.GenreEditViewImpl;
import com.otus.edu.library.client.form.genre.list.GenreListView;
import com.otus.edu.library.client.form.genre.list.GenreListViewImpl;
import com.otus.edu.library.client.form.login.LoginView;
import com.otus.edu.library.client.form.login.LoginViewImpl;
import com.otus.edu.library.client.form.register.RegisterView;
import com.otus.edu.library.client.form.register.RegisterViewImpl;
import com.otus.edu.library.client.main.MainView;
import com.otus.edu.library.client.main.MainViewImpl;

public class ClientFactory {
    private final EventBus eventBus = new SimpleEventBus();
    private final PlaceController placeController = new PlaceController(
            eventBus);
    // private final HelloView helloView = new HelloViewImpl();
    // private final GenreListView genreListView = new GenreListViewImpl();
    // private final AuthorListView authorListView = new AuthorListViewImpl();
    // private final BookListView bookListView = new BookListViewImpl();
    // private final GenreEditView genreEditView = new GenreEditViewImpl();
    // private final AuthorEditView authorEditView = new AuthorEditViewImpl();
    // private final BookEditView bookEditView = new BookEditViewImpl();
    // private final BookCommentsView bookCommentsView = new
    // BookCommentsViewImpl();

    public EventBus getEventBus() {
        return eventBus;
    }

    public PlaceController getPlaceController() {
        return placeController;
    }

    public LoginView getLoginView() {
        return new LoginViewImpl();
    }

    public MainView getHelloView() {
        return new MainViewImpl();
    }

    public GenreListView getGenreListView() {
        return new GenreListViewImpl();
    }

    public AuthorListView getAuthorListView() {
        return new AuthorListViewImpl();
    }

    public BookListView getBookListView() {
        return new BookListViewImpl();
    }

    public GenreEditView getGenreEditView() {
        return new GenreEditViewImpl();
    }

    public AuthorEditView getAuthorEditView() {
        return new AuthorEditViewImpl();
    }

    public BookEditView getBookEditView() {
        return new BookEditViewImpl();
    }

    public BookCommentsView getBookCommentsView() {
        return new BookCommentsViewImpl();
    }

    public RegisterView getRegisterView() {
        return new RegisterViewImpl();
    }

    public AdminView getAdminView() {
        return new AdminViewImpl();
    }

    public AccountEditView getAccountEditView() {
        return new AccountEditViewImpl();
    }
}
