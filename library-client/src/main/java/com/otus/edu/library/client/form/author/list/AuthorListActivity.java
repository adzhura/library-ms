package com.otus.edu.library.client.form.author.list;

import java.util.List;
import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.common.list.AbstractListActivity;
import com.otus.edu.library.client.place.AuthorEditPlace;
import com.otus.edu.library.client.place.AuthorListPlace;
import com.otus.edu.library.client.service.AuthorService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class AuthorListActivity extends AbstractListActivity<Author> {
    private ClientFactory clientFactory;

    public AuthorListActivity(AuthorListPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        AuthorListView view = clientFactory.getAuthorListView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setDeleteHandler(t -> {
            doDelete(t);
        });
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                    .goTo(new AuthorEditPlace(t.getId()));
        });
        view.createAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clientFactory.getPlaceController().goTo(new AuthorEditPlace());
            }
        });
    }

    @Override
    protected void doDelete(Author author) {
        REST.call(AuthorService.getInstance()).delete(author.getId(),
                RestCallback.create(
                        v -> AuthorListActivity.super.doDelete(author)));
    }

    @Override
    protected void loadData(Consumer<List<Author>> consumer) {
        REST.call(AuthorService.getInstance())
                .findAll(RestCallback.create(consumer));
    }

    @Override
    protected boolean matchBean(String text, Author bean) {
        return bean.getFirstName().contains(text)
                || bean.getLastName().contains(text);
    }

    public void goTo(Place place) {
        clientFactory.getPlaceController().goTo(place);
    }
}