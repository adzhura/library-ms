package com.otus.edu.library.client.form.author.edit;

import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.common.edit.AbstractEditActivity;
import com.otus.edu.library.client.place.AuthorEditPlace;
import com.otus.edu.library.client.place.AuthorListPlace;
import com.otus.edu.library.client.service.AuthorService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class AuthorEditActivity extends AbstractEditActivity {
    private ClientFactory clientFactory;
    private String id;

    public AuthorEditActivity(AuthorEditPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        AuthorEditView view = clientFactory.getAuthorEditView();
        containerWidget.setWidget(view.asWidget());
        
        initView(view);

        view.idDisplay().setValue(null);
        view.firstNameDisplay().setValue(null);
        view.lastNameDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty()) {
            loadData(id, author -> {
                view.saveEnabled().setEnabled(author != null);

                if (author != null) {
                    view.idDisplay().setValue(author.getId());
                    view.firstNameDisplay().setValue(author.getFirstName());
                    view.lastNameDisplay().setValue(author.getLastName());
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String id = view.idDisplay().getValue();
                id = "".equals(id) ? null : id;

                Author author = new Author(id,
                        view.firstNameDisplay().getValue(),
                        view.lastNameDisplay().getValue());

                saveData(author, r -> clientFactory.getPlaceController()
                        .goTo(new AuthorListPlace()));
            }
        });
    }

    private void loadData(String id, Consumer<Author> consumer) {
        REST.call(AuthorService.getInstance()).findOne(id, RestCallback.create(consumer));
    }

    private void saveData(Author author, Consumer<Void> consumer) {
        AuthorService service = REST.call(AuthorService.getInstance());

        if (author.getId() != null) {
            service.update(id.toString(), author,
                    RestCallback.create(consumer));
        } else {
            service.create(author,
                    RestCallback.create(r -> consumer.accept(null)));
        }
    }
}