package com.otus.edu.library.client.form.account.edit;

import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Account;
import com.otus.edu.library.client.common.edit.AbstractEditActivity;
import com.otus.edu.library.client.place.AccountEditPlace;
import com.otus.edu.library.client.place.GenreListPlace;
import com.otus.edu.library.client.service.AccountService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class AccountEditActivity extends AbstractEditActivity {
    private ClientFactory clientFactory;
    private String id;

    public AccountEditActivity(AccountEditPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        AccountEditView view = clientFactory.getAccountEditView();
        containerWidget.setWidget(view.asWidget());
        
        initView(view);

        view.idDisplay().setValue(null);
        view.nameDisplay().setValue(null);
        view.emailDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty()) {
            loadData(id, genre -> {
                view.saveEnabled().setEnabled(genre != null);

                if (genre != null) {
                    view.idDisplay().setValue(genre.getId());
                    view.nameDisplay().setValue(genre.getName());
                    view.emailDisplay().setValue(genre.getEmail());
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
//                String id = view.idDisplay().getValue();
//
//                Account account = new Account(id, view.nameDisplay().getValue(), null);
//
//                saveData(account, r -> clientFactory.getPlaceController()
//                        .goTo(new GenreListPlace()));
            }
        });
    }

    private void loadData(String id, Consumer<Account> consumer) {
        REST.call(AccountService.getInstance()).findOne(id, RestCallback.create(consumer));
    }

    private void saveData(Account account, Consumer<Void> consumer) {
        AccountService service = REST.call(AccountService.getInstance());

//        if (genre.getId() != null) {
//            service.update(id.toString(), genre, RestCallback.create(consumer));
//        } else {
//            service.create(genre,
//                    RestCallback.create(r -> consumer.accept(null)));
//        }
    }
}