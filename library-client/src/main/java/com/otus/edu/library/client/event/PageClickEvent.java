package com.otus.edu.library.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class PageClickEvent extends GwtEvent<PageClickHadler> {
    private static Type<PageClickHadler> TYPE;

    public static Type<PageClickHadler> getType() {
        if (TYPE == null) {
            TYPE = new Type<PageClickHadler>();
        }
        return TYPE;
    }

    private String token;

    public PageClickEvent(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public final Type<PageClickHadler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(PageClickHadler handler) {
        handler.onPageClick(this);
    }
}
