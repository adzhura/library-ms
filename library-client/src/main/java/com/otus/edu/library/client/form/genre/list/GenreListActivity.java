package com.otus.edu.library.client.form.genre.list;

import java.util.List;
import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.common.list.AbstractListActivity;
import com.otus.edu.library.client.place.GenreEditPlace;
import com.otus.edu.library.client.place.GenreListPlace;
import com.otus.edu.library.client.service.GenreService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class GenreListActivity extends AbstractListActivity<Genre> {
    private ClientFactory clientFactory;

    public GenreListActivity(GenreListPlace place,
            ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        GenreListView view = clientFactory.getGenreListView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setDeleteHandler(t -> {
            doDelete(t);
        });
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                    .goTo(new GenreEditPlace(t.getId()));
        });
        view.createAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clientFactory.getPlaceController().goTo(new GenreEditPlace());
            }
        });
    }

    @Override
    protected void doDelete(Genre genre) {
        REST.call(GenreService.getInstance()).delete(genre.getId(), RestCallback
                .create(v -> GenreListActivity.super.doDelete(genre)));
    }

    @Override
    protected void loadData(Consumer<List<Genre>> consumer) {
        REST.call(GenreService.getInstance())
                .findAll(RestCallback.create(consumer));
    }

    @Override
    protected boolean matchBean(String text, Genre bean) {
        return bean.getName().contains(text);
    }
}