package com.otus.edu.library.client.service;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;
import com.otus.edu.library.client.bean.Account;
import com.otus.edu.library.client.bean.ApiResponse;
import com.otus.edu.library.client.bean.LoginInfo;

@Path("/account")
public interface AccountService extends RestService {
    public static AccountService getInstance() {
        Resource resource = new Resource(ServiceConfig.adminUrl,
                new HashMap<>());

        AccountService service = GWT.create(AccountService.class);
        ((RestServiceProxy) service).setResource(resource);

        return service;
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public void register(LoginInfo loginInfo, MethodCallback<ApiResponse> callback);

    @GET
    @Path("/current")
    @Produces(MediaType.APPLICATION_JSON)
    public void currentAccount(MethodCallback<Account> callback);

    @GET()
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public void findAll(MethodCallback<List<Account>> callback);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void findOne(@PathParam("id") String id,
            MethodCallback<Account> callback);
}
