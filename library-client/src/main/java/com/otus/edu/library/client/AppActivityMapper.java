package com.otus.edu.library.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.otus.edu.library.client.form.account.edit.AccountEditActivity;
import com.otus.edu.library.client.form.admin.AdminActivity;
import com.otus.edu.library.client.form.author.edit.AuthorEditActivity;
import com.otus.edu.library.client.form.author.list.AuthorListActivity;
import com.otus.edu.library.client.form.book.comments.BookCommentsActivity;
import com.otus.edu.library.client.form.book.edit.BookEditActivity;
import com.otus.edu.library.client.form.book.list.BookListActivity;
import com.otus.edu.library.client.form.genre.edit.GenreEditActivity;
import com.otus.edu.library.client.form.genre.list.GenreListActivity;
import com.otus.edu.library.client.form.login.LoginActivity;
import com.otus.edu.library.client.form.register.RegisterActivity;
import com.otus.edu.library.client.main.MainActivity;
import com.otus.edu.library.client.place.AccountEditPlace;
import com.otus.edu.library.client.place.AdminPlace;
import com.otus.edu.library.client.place.AuthorEditPlace;
import com.otus.edu.library.client.place.AuthorListPlace;
import com.otus.edu.library.client.place.BookCommentsPlace;
import com.otus.edu.library.client.place.BookEditPlace;
import com.otus.edu.library.client.place.BookListPlace;
import com.otus.edu.library.client.place.GenreEditPlace;
import com.otus.edu.library.client.place.GenreListPlace;
import com.otus.edu.library.client.place.LoginPlace;
import com.otus.edu.library.client.place.MainPlace;
import com.otus.edu.library.client.place.RegisterPlace;

public class AppActivityMapper implements ActivityMapper {
    private ClientFactory clientFactory;

    public AppActivityMapper(ClientFactory clientFactory) {
        super();
        this.clientFactory = clientFactory;
    }

    @Override
    public Activity getActivity(Place place) {
        if (place instanceof MainPlace)
            return new MainActivity((MainPlace) place, clientFactory);
        else if (place instanceof LoginPlace)
            return new LoginActivity((LoginPlace) place, clientFactory);
        else if (place instanceof RegisterPlace)
            return new RegisterActivity((RegisterPlace) place, clientFactory);
        else if (place instanceof AdminPlace)
            return new AdminActivity((AdminPlace) place, clientFactory);
        else if (place instanceof AccountEditPlace)
            return new AccountEditActivity((AccountEditPlace) place,
                    clientFactory);

        else if (place instanceof GenreListPlace)
            return new GenreListActivity((GenreListPlace) place, clientFactory);
        else if (place instanceof AuthorListPlace)
            return new AuthorListActivity((AuthorListPlace) place,
                    clientFactory);
        else if (place instanceof BookListPlace)
            return new BookListActivity((BookListPlace) place, clientFactory);
        else if (place instanceof GenreEditPlace)
            return new GenreEditActivity((GenreEditPlace) place, clientFactory);
        else if (place instanceof AuthorEditPlace)
            return new AuthorEditActivity((AuthorEditPlace) place,
                    clientFactory);
        else if (place instanceof BookEditPlace)
            return new BookEditActivity((BookEditPlace) place, clientFactory);
        else if (place instanceof BookCommentsPlace)
            return new BookCommentsActivity((BookCommentsPlace) place,
                    clientFactory);
        
        return null;
    }
}