package com.otus.edu.library.client.common;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasEnabled;

interface Action extends HasClickHandlers, HasEnabled {

}
