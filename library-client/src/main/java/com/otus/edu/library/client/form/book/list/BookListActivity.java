package com.otus.edu.library.client.form.book.list;

import java.util.List;
import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Book;
import com.otus.edu.library.client.common.list.AbstractListActivity;
import com.otus.edu.library.client.place.BookEditPlace;
import com.otus.edu.library.client.place.BookListPlace;
import com.otus.edu.library.client.service.BookService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class BookListActivity extends AbstractListActivity<Book> {
    private ClientFactory clientFactory;

    public BookListActivity(BookListPlace place, ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        BookListView view = clientFactory.getBookListView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setDeleteHandler(t -> {
            doDelete(t);
        });
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                    .goTo(new BookEditPlace(t.getId()));
        });
        view.createAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clientFactory.getPlaceController().goTo(new BookEditPlace());
            }
        });
    }

    @Override
    protected void doDelete(Book book) {
        REST.call(BookService.getInstance()).delete(book.getId(), RestCallback
                .create(v -> BookListActivity.super.doDelete(book)));
    }

    @Override
    protected void loadData(Consumer<List<Book>> consumer) {
        REST.call(BookService.getInstance())
                .findAll(RestCallback.create(consumer));
    }

    @Override
    protected boolean matchBean(String text, Book bean) {
        return bean.getName().contains(text)
                || bean.getAuthor().getFirstName().contains(text)
                || bean.getAuthor().getLastName().contains(text)
                || bean.getGenre().getName().contains(text);
    }

    public void goTo(Place place) {
        clientFactory.getPlaceController().goTo(place);
    }
}