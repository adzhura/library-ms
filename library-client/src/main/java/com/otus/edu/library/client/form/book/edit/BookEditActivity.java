package com.otus.edu.library.client.form.book.edit;

import java.util.List;
import java.util.function.Consumer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.bean.Book;
import com.otus.edu.library.client.bean.Genre;
import com.otus.edu.library.client.common.edit.AbstractEditActivity;
import com.otus.edu.library.client.place.BookEditPlace;
import com.otus.edu.library.client.place.BookListPlace;
import com.otus.edu.library.client.service.AuthorService;
import com.otus.edu.library.client.service.BookService;
import com.otus.edu.library.client.service.GenreService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class BookEditActivity extends AbstractEditActivity {
    private ClientFactory clientFactory;
    private String id;

    public BookEditActivity(BookEditPlace place, ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        BookEditView view = clientFactory.getBookEditView();
        containerWidget.setWidget(view.asWidget());
        
        initView(view);

        view.idDisplay().setValue(null);
        view.nameDisplay().setValue(null);
        view.authorDisplay().setValue(null);
        view.genreDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty()) {
            loadData(id, book -> {
                view.saveEnabled().setEnabled(book != null);

                if (book != null) {
                    view.idDisplay().setValue(book.getId());
                    view.nameDisplay().setValue(book.getName());
                    view.authorDisplay().setValue(book.getAuthor(), true);
                    view.genreDisplay().setValue(book.getGenre(), true);
                }

                updateLookupAuthor(view, r -> {
                });
                updateLookupGenre(view, r -> {
                });
            });
        } else {
            updateLookupAuthor(view, r -> {
                if (r != null && r.size() > 0) {
                    view.authorDisplay().setValue(r.get(0));
                }
            });
            updateLookupGenre(view, r -> {
                if (r != null && r.size() > 0) {
                    view.genreDisplay().setValue(r.get(0));
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String id = view.idDisplay().getValue();
                id = "".equals(id) ? null : id;

                Book book = new Book(id, view.nameDisplay().getValue(),
                        view.authorDisplay().getValue(),
                        view.genreDisplay().getValue(), null);

                saveData(book, r -> clientFactory.getPlaceController()
                        .goTo(new BookListPlace()));
            }
        });
    }

    private void updateLookupAuthor(BookEditView view,
            Consumer<List<Author>> consumer) {
        REST.call(AuthorService.getInstance())
                .findAll(RestCallback.create(authors -> {
                    consumer.accept(authors);
                    view.authorDisplay().setAcceptableValues(authors);
                }));
    }

    private void updateLookupGenre(BookEditView view,
            Consumer<List<Genre>> consumer) {
        REST.call(GenreService.getInstance())
                .findAll(RestCallback.create(genres -> {
                    consumer.accept(genres);
                    view.genreDisplay().setAcceptableValues(genres);
                }));
    }

    private void loadData(String id, Consumer<Book> consumer) {
        REST.call(BookService.getInstance()).findOne(id,
                RestCallback.create(consumer));
    }

    private void saveData(Book book, Consumer<Void> consumer) {
        BookService service = REST.call(BookService.getInstance());

        if (book.getId() != null) {
            service.update(id.toString(), book, RestCallback.create(consumer));
        } else {
            service.create(book,
                    RestCallback.create(r -> consumer.accept(null)));
        }
    }
}