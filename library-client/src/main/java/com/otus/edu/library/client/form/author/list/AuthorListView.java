package com.otus.edu.library.client.form.author.list;

import com.otus.edu.library.client.bean.Author;
import com.otus.edu.library.client.common.list.ListView;

public interface AuthorListView extends ListView<Author> {
}