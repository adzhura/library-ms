package com.otus.edu.library.client.service;

public interface ServiceConfig {
    int port = 8762;// 8200;
    String library = "/library";
    String auth = "/auth";
    String admin = "/account";
    String host = "http://localhost:" + port;
    String libraryUrl = "http://localhost:" + port + library;
    String authUrl = "http://localhost:" + port + auth;
    String adminUrl = "http://localhost:" + port + admin;
}
