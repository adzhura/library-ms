package com.otus.edu.library.client.form.register;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;

public interface RegisterView extends IsWidget {
    void setPresenter(Presenter presenter);

    public interface Presenter {
        void goTo(Place place);
    }

    HasValue<String> userField();

    HasValue<String> passwordField();

    HasValue<String> passwordConfirmField();

    HasClickHandlers registerAction();
}
