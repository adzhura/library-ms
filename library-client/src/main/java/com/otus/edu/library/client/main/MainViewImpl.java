package com.otus.edu.library.client.main;

import java.util.Collection;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.otus.edu.library.client.bean.Page;

public class MainViewImpl extends Composite implements MainView {
    private static HelloViewImplUiBinder uiBinder = GWT
            .create(HelloViewImplUiBinder.class);

    interface HelloViewImplUiBinder extends UiBinder<Widget, MainViewImpl> {
    }

    @UiField
    FlowPanel linkContainer;

    @UiField
    FlowPanel adminContainer;

    @UiField
    Anchor logoutLink;

    public MainViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
        logoutLink.setVisible(false);
    }

    @Override
    public void setPresenter(Presenter presenter) {
    }

    @Override
    public void addLink(Page page) {
        String href = Window.Location.getHref();
        int loc = href.indexOf('#');

        if (loc > 0) {
            href = href.substring(0, loc);
        }

        Anchor anchor = new Anchor(page.getName(),
                href + "#" + page.getToken() + ":");

        linkContainer.add(new SimplePanel(anchor));
    }
    
    @Override
    public HasClickHandlers logoutAction() {
        return logoutLink;
    }
}