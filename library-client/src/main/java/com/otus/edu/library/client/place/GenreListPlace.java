package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class GenreListPlace extends Place {
    public static final String TOKEN = "genres";
    
    public GenreListPlace() {
    }

    @Prefix(TOKEN)
    public static class Tokenizer implements PlaceTokenizer<GenreListPlace> {
        @Override
        public String getToken(GenreListPlace place) {
            return "";
        }

        @Override
        public GenreListPlace getPlace(String token) {
            return new GenreListPlace();
        }
    }
}