package com.otus.edu.library.client.form.login;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;

public interface LoginView extends IsWidget {
    enum Mode {
        LOGIN, CONFIRM
    }

    void setPresenter(Presenter presenter);

    public interface Presenter {
        void goTo(Place place);
    }

    HasValue<String> userField();

    HasValue<String> passwordField();

    HasValue<String> codeField();

    HasClickHandlers loginAction();

    HasClickHandlers confirmAction();

    HasClickHandlers registerAction();

    void setMode(Mode mode);
}
