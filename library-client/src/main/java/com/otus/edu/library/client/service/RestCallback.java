package com.otus.edu.library.client.service;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.http.client.Response;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Window;

public abstract class RestCallback<T> implements MethodCallback<T> {

    @SuppressWarnings("unused")
    private PlaceController placeController;

    public RestCallback(PlaceController placeController) {
        super();
        this.placeController = placeController;
    }

    @Override
    public void onFailure(Method method, Throwable exception) {
        Response response = method.getResponse();

        if (response.getStatusCode() == 0) {
            Window.alert("Service unavailable.");
            gotoLogin();
        } else if (response.getStatusCode() == 401) {
            Window.alert("You are not logged");
            gotoLogin();
        } else {
            Window.alert(exception.getMessage() + "\n" + "statusCode: "
                    + response.getStatusCode());
        }
    }

    private void gotoLogin() {
        String href = Window.Location.getHref();
        int loc = href.indexOf('#');

        if (loc > 0) {
            href = href.substring(0, loc);
        }

        Window.Location.replace(href + "#login:");
        // Window.Location.assign(href + "#login:");
        // placeController.goTo(new LoginPlace());
    }

    public static <R> RestCallback<R> create(Consumer<R> consumer) {
        return new RestCallback<R>(null) {
            @Override
            public void onSuccess(Method method, R response) {
                consumer.accept(response);
            }
        };
    }

    public static <R> RestCallback<R> create(BiConsumer<Method, R> consumer) {
        return new RestCallback<R>(null) {
            @Override
            public void onSuccess(Method method, R response) {
                consumer.accept(method, response);
            }
        };
    }
}
