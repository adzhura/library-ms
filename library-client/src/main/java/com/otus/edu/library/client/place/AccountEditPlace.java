package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class AccountEditPlace extends Place {
    private String id;

    public AccountEditPlace(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Prefix("accountEdit")
    public static class Tokenizer implements PlaceTokenizer<AccountEditPlace> {
        @Override
        public String getToken(AccountEditPlace place) {
            return place.getId();
        }

        @Override
        public AccountEditPlace getPlace(String token) {
            return new AccountEditPlace(token);
        }
    }
}