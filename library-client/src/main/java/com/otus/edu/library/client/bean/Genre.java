package com.otus.edu.library.client.bean;

public class Genre implements NamedObject<String> {
    private String id;

    private String name;

    public Genre() {
    }

    public Genre(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
