package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class MainPlace extends Place {
    public MainPlace() {
    }

    public static class Tokenizer implements PlaceTokenizer<MainPlace> {
        @Override
        public String getToken(MainPlace place) {
            return "";
        }

        @Override
        public MainPlace getPlace(String token) {
            return new MainPlace();
        }
    }
}