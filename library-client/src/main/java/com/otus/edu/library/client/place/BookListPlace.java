package com.otus.edu.library.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class BookListPlace extends Place {
    public static final String TOKEN = "books";

    public BookListPlace() {
    }

    @Prefix(TOKEN)
    public static class Tokenizer implements PlaceTokenizer<BookListPlace> {
        @Override
        public String getToken(BookListPlace place) {
            return "";
        }

        @Override
        public BookListPlace getPlace(String token) {
            return new BookListPlace();
        }
    }
}