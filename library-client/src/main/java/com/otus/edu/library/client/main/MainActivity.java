package com.otus.edu.library.client.main;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.HasVisibility;
import com.otus.edu.library.client.ClientFactory;
import com.otus.edu.library.client.bean.Page;
import com.otus.edu.library.client.place.AdminPlace;
import com.otus.edu.library.client.place.AuthorListPlace;
import com.otus.edu.library.client.place.BookListPlace;
import com.otus.edu.library.client.place.GenreListPlace;
import com.otus.edu.library.client.place.LoginPlace;
import com.otus.edu.library.client.place.MainPlace;
import com.otus.edu.library.client.service.AccountService;
import com.otus.edu.library.client.service.REST;
import com.otus.edu.library.client.service.RestCallback;

public class MainActivity extends AbstractActivity
        implements MainView.Presenter {
    // Used to obtain views, eventBus, placeController
    // Alternatively, could be injected via GIN
    private ClientFactory clientFactory;

    public MainActivity(MainPlace place, ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    /**
     * Invoked by the ActivityManager to start a new Activity
     */
    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
        MainView view = clientFactory.getHelloView();
        view.setPresenter(this);
        containerWidget.setWidget(view.asWidget());

        view.logoutAction().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Cookies.removeCookie("Authorization");
                clientFactory.getPlaceController().goTo(new LoginPlace());
            }

        });

        loadPages(view);
    }

    private void loadPages(MainView view) {
        REST.call(AccountService.getInstance())
                .currentAccount(RestCallback.create(account -> {

                    view.addLink(new Page("Genres", GenreListPlace.TOKEN));
                    view.addLink(new Page("Authors", AuthorListPlace.TOKEN));
                    view.addLink(new Page("Books", BookListPlace.TOKEN));

                    if (account.getRoles().contains("ADMIN")) {
                        view.addLink(new Page("Admin", AdminPlace.TOKEN));
                    }
                    
                    if (view.logoutAction() instanceof HasVisibility) {
                        ((HasVisibility)view.logoutAction()).setVisible(true); 
                    }
                }));
    }

    /**
     * Navigate to a new Place in the browser
     */
    @Override
    public void goTo(Place place) {
        clientFactory.getPlaceController().goTo(place);
    }
}