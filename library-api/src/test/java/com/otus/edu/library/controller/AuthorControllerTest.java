package com.otus.edu.library.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otus.edu.library.controller.AuthorController;
import com.otus.edu.library.domain.Author;
import com.otus.edu.library.dto.AuthorDto;
import com.otus.edu.library.repository.AuthorRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthorController.class)
public class AuthorControllerTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private AuthorRepository authorRepository;

	private final String baseUrl = "/authors/";
	private Author author;

	@Before
	public void init() {
		author = new Author("Joe", "Black");
		author.setId(1);
		ControllerTestUtil.configureRepositoryMock(authorRepository,
		        Integer.class, author);
	}

	@Test
	public void findAll() throws Exception {
		mvc.perform(get(baseUrl)).andExpect(jsonPath("$", hasSize(1)))
		        .andExpect(jsonPath("$[0].id", is(author.getId())))
		        .andExpect(
		                jsonPath("$[0].firstName", is(author.getFirstName())))
		        .andExpect(jsonPath("$[0].lastName", is(author.getLastName())));
	}

	@Test
	public void findOne() throws Exception {
		mvc.perform(get(baseUrl + "/{id}", author.getId()))
		        .andExpect(jsonPath("$.id").value(author.getId()))
		        .andExpect(jsonPath("$.firstName", is(author.getFirstName())))
		        .andExpect(jsonPath("$.lastName", is(author.getLastName())));
	}

	@Test
	public void create() throws Exception {
		AuthorDto newAuthor = new AuthorDto(Integer.MAX_VALUE, "new FirstName",
		        "new LastName");
		String authorJson = new ObjectMapper().writeValueAsString(newAuthor);
		mvc.perform(post(baseUrl).contentType(MediaType.APPLICATION_JSON)
		        .content(authorJson)).andExpect(status().isCreated());

		mvc.perform(get(baseUrl + "/{id}", newAuthor.getId()))
		        .andExpect(jsonPath("$.id").value(newAuthor.getId()))
		        .andExpect(
		                jsonPath("$.firstName", is(newAuthor.getFirstName())))
		        .andExpect(jsonPath("$.lastName", is(newAuthor.getLastName())));
	}

	@Test
	public void update() throws Exception {
		AuthorDto newAuthor = new AuthorDto(1, "update FirstName",
		        "update LastName");
		String authorJson = new ObjectMapper().writeValueAsString(newAuthor);
		mvc.perform(put(baseUrl + "/{id}", author.getId())
		        .contentType(MediaType.APPLICATION_JSON).content(authorJson))
		        .andExpect(status().isOk());
	}

	@Test
	public void _delete() throws Exception {
		mvc.perform(delete(baseUrl + "/{id}", author.getId()))
		        .andExpect(status().isOk());
	}
}
