package com.otus.edu.library.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doAnswer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.edu.library.domain.HasId;

public class ControllerTestUtil {
	@SuppressWarnings("unchecked")
	public static <T extends HasId<ID>, ID, REPO extends JpaRepository<T, ID>> void configureRepositoryMock(
	        REPO repository, Class<ID> idClass, T testBean) {
		configureRepositoryMock(repository, idClass,
		        (Class<T>) testBean.getClass(), Arrays.asList(testBean));
	}

	public static <T extends HasId<ID>, ID, REPO extends JpaRepository<T, ID>> void configureRepositoryMock(
	        REPO repository, Class<ID> idClass, Class<T> beanClass) {
		configureRepositoryMock(repository, idClass, beanClass,
		        new ArrayList<>());
	}

	private static <T extends HasId<ID>, ID, REPO extends JpaRepository<T, ID>> void configureRepositoryMock(
	        REPO repository, Class<ID> idClass, Class<T> beanClass,
	        List<T> initialBeans) {
		List<T> beans = new ArrayList<>(initialBeans);

		given(repository.findAll()).willReturn(beans);

		doAnswer(invocation -> {
			ID id = invocation.getArgument(0);
			return beans.stream().filter(e -> Objects.equals(id, e.getId()))
			        .findFirst();
		}).when(repository).findById(any(idClass));

		doAnswer(invocation -> {
			T t = invocation.getArgument(0);

			if (!beans.contains(t)) {
				beans.add(t);
			}

			return t;
		}).when(repository).save(any(beanClass));

		doAnswer(invocation -> {
			T t = invocation.getArgument(0);
			beans.remove(t);
			return null;
		}).when(repository).delete(any(beanClass));
	}
}
