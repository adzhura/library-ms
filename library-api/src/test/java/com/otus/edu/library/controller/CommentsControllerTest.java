package com.otus.edu.library.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otus.edu.library.controller.BookController;
import com.otus.edu.library.controller.CommentController;
import com.otus.edu.library.domain.Author;
import com.otus.edu.library.domain.Book;
import com.otus.edu.library.domain.Comment;
import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.dto.CommentDto;
import com.otus.edu.library.repository.BookRepository;
import com.otus.edu.library.repository.CommentRepository;
import com.otus.edu.library.service.CommentService;

@RunWith(SpringRunner.class)
@WebMvcTest({ CommentController.class, BookController.class })
public class CommentsControllerTest {
	@TestConfiguration
	static class Config extends BookControllerTest.Config {
		@MockBean
		private CommentRepository commentRepository;

		@Bean
		public CommentRepository commentRepository() {
			return commentRepository;
		}

		@Bean
		public CommentService commentService() {
			CommentService commentService = new CommentService(bookRepository(),
			        commentRepository());

			return commentService;
		}
	}

	@Autowired
	private MockMvc mvc;

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private CommentRepository commentRepository;

	private final String baseUrl = "/books";

	private Book book;
	private Comment comment;

	@Before
	public void init() {
		Author author = new Author("Joe", "Black");
		author.setId(1);

		Genre genre = new Genre("Test genre");
		genre.setId(1);

		book = new Book(author, genre, "Test book");
		book.setId(1);
		ControllerTestUtil.configureRepositoryMock(bookRepository,
		        Integer.class, book);

		comment = new Comment(book, "anonymous", "good");
		book.setComments(new ArrayList<>(Arrays.asList(comment)));

		ControllerTestUtil.configureRepositoryMock(commentRepository,
		        Integer.class, Comment.class);
	}

	@Test
	public void findBook() throws Exception {
		mvc.perform(get(baseUrl + "/{id}", book.getId()))
		        .andExpect(jsonPath("$.id").value(book.getId()))
		        .andExpect(jsonPath("$.name").value(book.getName()))
		        .andExpect(
		                jsonPath("$.author.id").value(book.getAuthor().getId()))
		        .andExpect(jsonPath("$.author.firstName")
		                .value(book.getAuthor().getFirstName()))
		        .andExpect(jsonPath("$.author.lastName")
		                .value(book.getAuthor().getLastName()))
		        .andExpect(
		                jsonPath("$.genre.id").value(book.getGenre().getId()))
		        .andExpect(jsonPath("$.genre.name")
		                .value(book.getGenre().getName()))
		        .andExpect(jsonPath("$.comments", hasSize(1)));
	}

	@Test
	public void create() throws Exception {
		CommentDto newComment = new CommentDto(1, "user name", "another text");
		String commentJson = new ObjectMapper().writeValueAsString(newComment);

		mvc.perform(post(baseUrl + "/{id}/comments", book.getId())
		        .contentType(MediaType.APPLICATION_JSON).content(commentJson))
		        .andExpect(status().isCreated());

		mvc.perform(get(baseUrl + "/{id}", book.getId()))
		        .andExpect(jsonPath("$.id").value(book.getId()))
		        .andExpect(jsonPath("$.name").value(book.getName()))
		        .andExpect(
		                jsonPath("$.author.id").value(book.getAuthor().getId()))
		        .andExpect(jsonPath("$.author.firstName")
		                .value(book.getAuthor().getFirstName()))
		        .andExpect(jsonPath("$.author.lastName")
		                .value(book.getAuthor().getLastName()))
		        .andExpect(
		                jsonPath("$.genre.id").value(book.getGenre().getId()))
		        .andExpect(jsonPath("$.genre.name")
		                .value(book.getGenre().getName()))
		        .andExpect(jsonPath("$.comments", hasSize(2)))
		        .andExpect(jsonPath("$.comments[1].userName")
		                .value(newComment.getUserName()))
		        .andExpect(jsonPath("$.comments[1].text")
		                .value(newComment.getText()));
	}
}
