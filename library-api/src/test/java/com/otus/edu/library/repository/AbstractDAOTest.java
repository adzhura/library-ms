package com.otus.edu.library.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.otus.edu.library.domain.HasId;
import com.otus.edu.library.repository.NamedObjectRepository;

public class AbstractDAOTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	protected <T extends HasId<ID>, ID> void testCreate(
	        CrudRepository<T, ID> dao, T object) {
		dao.save(object);
		Optional<T> testObject = dao.findById(object.getId());
		dao.delete(object);
		assertThat(testObject).hasValue(object);
	}

	protected <T extends HasId<ID>, ID> void testUpdate(
	        CrudRepository<T, ID> dao, T createObject, T updateObject) {
		dao.save(createObject);
		updateObject.setId(createObject.getId());
		dao.save(updateObject);
		Optional<T> testObject = dao.findById(createObject.getId());
		dao.delete(createObject);
		assertThat(testObject).hasValue(updateObject);
	}

	protected <T extends HasId<ID>, ID> void testDelete(
	        CrudRepository<T, ID> dao, T object) {
		dao.save(object);
		Optional<T> testObject = dao.findById(object.getId());
		dao.delete(object);
		assertThat(testObject).hasValue(object);
		assertThat(dao.findById(object.getId())).isEmpty();
	}

	protected void testFindAll(JpaRepository<?, ?> dao, int count) {
		assertThat(dao.count()).isEqualTo(count);
		assertThat(dao.findAll().size()).isEqualTo(count);
	}

	protected <T extends HasId<ID>, ID> void testFindById(
	        CrudRepository<T, ID> dao, ID id) {
		Optional<T> object = dao.findById(id);
		assertThat(object).isNotEmpty();
		assertThat(object.get().getId()).isEqualTo(id);
	}

	protected <T> void testFindByName(NamedObjectRepository<T> dao, String name,
	        Predicate<T> predicate) {
		Collection<T> objects = dao.findByName(name);
		assertEquals(objects.size(), 1);
		objects.forEach(o -> assertTrue(predicate.test(o)));
	}
}
