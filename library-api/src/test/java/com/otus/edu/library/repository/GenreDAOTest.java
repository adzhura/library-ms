package com.otus.edu.library.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.repository.GenreRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GenreDAOTest extends AbstractDAOTest {
	@Autowired
	private GenreRepository dao;

	@Autowired
	private Genre genreCreate;
	@Autowired
	private Genre genreUpdate;

	@Test
	public void findAll() {
		testFindAll(dao, 2);
	}

	@Test
	public void findById() {
		testFindById(dao, 1);
	}

	@Test
	public void findByName() {
		testFindByName(dao, "Юмор", t -> "Юмор".equals(t.getName()));
	}

	@Test
	public void create() {
		testCreate(dao, genreCreate);
	}

	@Test
	public void update() {
		testUpdate(dao, genreCreate, genreUpdate);
	}

	@Test
	public void delete() {
		testDelete(dao, genreCreate);
	}
}
