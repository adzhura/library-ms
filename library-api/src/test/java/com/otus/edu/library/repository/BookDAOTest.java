package com.otus.edu.library.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.otus.edu.library.domain.Author;
import com.otus.edu.library.domain.Book;
import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.repository.AuthorRepository;
import com.otus.edu.library.repository.BookRepository;
import com.otus.edu.library.repository.GenreRepository;
import com.otus.edu.library.service.BookService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookDAOTest extends AbstractDAOTest {
	@Autowired
	private BookRepository bookDao;

	@Autowired
	private GenreRepository genreDao;

	@Autowired
	private AuthorRepository authorDao;

	@Autowired
	private Book bookCreate;
	@Autowired
	private Book bookUpdate;

	@Autowired
	private BookService bookService;

	private Optional<Genre> genre;
	private Optional<Author> author;

	@Before
	public void init() {
		genre = genreDao.findById(1);
		author = authorDao.findById(1);
	}

	@Test
	public void findAll() {
		testFindAll(bookDao, 4);
	}

	@Test
	public void findById() {
		testFindById(bookDao, 1);
	}

	@Test
	public void findByName() {
		testFindByName(bookDao, "Гарри Поттер",
		        t -> "Гарри Поттер".equals(t.getName()));
	}

	@Test
	public void create() {
		bookCreate.setAuthor(author.get());
		bookCreate.setGenre(genre.get());
		testCreate(bookDao, bookCreate);

		Book anotherBook = bookService.createBook(2, 2, "another");
		Optional<Book> findBook = bookDao.findById(anotherBook.getId());
		assertThat(anotherBook).isNotNull();
		assertThat(findBook).isNotEmpty();
		assertThat(anotherBook).isEqualTo(findBook.get());
		bookDao.delete(findBook.get());
	}

	@Test
	public void update() {
		bookCreate.setAuthor(author.get());
		bookCreate.setGenre(genre.get());
		bookUpdate.setAuthor(author.get());
		bookUpdate.setGenre(genre.get());
		testUpdate(bookDao, bookCreate, bookUpdate);
	}

	@Test
	public void delete() {
		bookCreate.setAuthor(author.get());
		bookCreate.setGenre(genre.get());
		testDelete(bookDao, bookCreate);
	}
}
