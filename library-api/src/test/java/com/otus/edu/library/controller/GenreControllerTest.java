package com.otus.edu.library.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.otus.edu.library.controller.GenreController;
import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.dto.GenreDto;
import com.otus.edu.library.repository.GenreRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(GenreController.class)
public class GenreControllerTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private GenreRepository genreRepository;

	private final String baseUrl = "/genres";
	private Genre genre;

	@Before
	public void init() {
		genre = new Genre("Test genre");
		genre.setId(1);
		ControllerTestUtil.configureRepositoryMock(genreRepository,
		        Integer.class, genre);
	}

	@Test
	public void findAll() throws Exception {
		// Test json content
		String content = mvc.perform(get(baseUrl)).andReturn().getResponse()
		        .getContentAsString();

		List<Genre> genres = new ObjectMapper().readValue(content,
		        new TypeReference<List<Genre>>() {
		        });

		assertThat(genres).isEqualTo(Arrays.asList(genre));

		// Test json path
		mvc.perform(get(baseUrl)).andExpect(jsonPath("$", hasSize(1)))
		        .andExpect(jsonPath("$[0].id", is(genre.getId())))
		        .andExpect(jsonPath("$[0].name", is(genre.getName())));
	}

	@Test
	public void findOne() throws Exception {
		mvc.perform(get(baseUrl + "/{id}", genre.getId()))
		        .andExpect(jsonPath("$.id").value(genre.getId()))
		        .andExpect(jsonPath("$.name").value(genre.getName()));
	}

	@Test
	public void create() throws Exception {
		GenreDto newGenre = new GenreDto(Integer.MAX_VALUE, "New genre");
		String genreJson = new ObjectMapper().writeValueAsString(newGenre);
		mvc.perform(post(baseUrl).contentType(MediaType.APPLICATION_JSON)
		        .content(genreJson)).andExpect(status().isCreated());

		mvc.perform(get(baseUrl + "/{id}", newGenre.getId()))
		        .andExpect(jsonPath("$.id").value(newGenre.getId()))
		        .andExpect(jsonPath("$.name").value(newGenre.getName()));
	}

	@Test
	public void update() throws Exception {
		GenreDto newGenre = new GenreDto(1, "update genre");
		String genreJson = new ObjectMapper().writeValueAsString(newGenre);

		mvc.perform(put(baseUrl + "/{id}", genre.getId())
		        .contentType(MediaType.APPLICATION_JSON).content(genreJson))
		        .andExpect(status().isOk());
	}

	@Test
	public void _delete() throws Exception {
		mvc.perform(delete(baseUrl + "/{id}", genre.getId()))
		        .andExpect(status().isOk());
	}
}
