package com.otus.edu.library.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otus.edu.library.controller.BookController;
import com.otus.edu.library.domain.Author;
import com.otus.edu.library.domain.Book;
import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.dto.AuthorDto;
import com.otus.edu.library.dto.BookDto;
import com.otus.edu.library.dto.GenreDto;
import com.otus.edu.library.repository.AuthorRepository;
import com.otus.edu.library.repository.BookRepository;
import com.otus.edu.library.repository.GenreRepository;
import com.otus.edu.library.service.BookService;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {
	@TestConfiguration
	public static class Config {
		@MockBean
		private GenreRepository genreRepository;
		@MockBean
		private AuthorRepository authorRepository;
		@MockBean
		private BookRepository bookRepository;

		@Bean
		public GenreRepository genreRepository() {
			return genreRepository;
		}

		@Bean
		public AuthorRepository authorRepository() {
			return authorRepository;
		}

		@Bean
		public BookRepository bookRepository() {
			return bookRepository;
		}

		@Bean
		public BookService bookService() {
			BookService bookService = new BookService(authorRepository(),
			        genreRepository(), bookRepository());
			BookService spy = Mockito.spy(bookService);

			doAnswer(new BookServiceAnswer()).when(spy).createBook(anyInt(),
			        anyInt(), any(String.class));

			return spy;
		}

		private static class BookServiceAnswer implements Answer<Book> {
			@Override
			public Book answer(InvocationOnMock invocation) throws Throwable {
				Book result = (Book) invocation.callRealMethod();

				if (result != null && result.getComments() == null) {
					// result.setComments(new ArrayList<>());
				}

				return result;
			}
		}
	}

	@Autowired
	private MockMvc mvc;

	@Autowired
	private GenreRepository genreRepository;
	@Autowired
	private AuthorRepository authorRepository;
	@Autowired
	private BookRepository bookRepository;

	private final String baseUrl = "/books/";
	private Book book;

	private Author author;

	private Genre genre;

	@Before
	public void init() {
		author = new Author("Joe", "Black");
		author.setId(1);
		ControllerTestUtil.configureRepositoryMock(authorRepository,
		        Integer.class, author);

		genre = new Genre("Test genre");
		genre.setId(1);
		ControllerTestUtil.configureRepositoryMock(genreRepository,
		        Integer.class, genre);

		book = new Book(author, genre, "Test book");
		book.setId(1);
		book.setComments(new ArrayList<>());
		ControllerTestUtil.configureRepositoryMock(bookRepository,
		        Integer.class, book);
	}

	@Test
	public void findAll() throws Exception {
		mvc.perform(get(baseUrl)).andExpect(jsonPath("$", hasSize(1)))
		        .andExpect(jsonPath("$[0].id", is(book.getId())))
		        .andExpect(jsonPath("$[0].name", is(book.getName())));
	}

	@Test
	public void findOne() throws Exception {
		mvc.perform(get(baseUrl + "/{id}", book.getId()))
		        .andExpect(jsonPath("$.id").value(book.getId()))
		        .andExpect(jsonPath("$.name").value(book.getName()))
		        .andExpect(
		                jsonPath("$.author.id").value(book.getAuthor().getId()))
		        .andExpect(jsonPath("$.author.firstName")
		                .value(book.getAuthor().getFirstName()))
		        .andExpect(jsonPath("$.author.lastName")
		                .value(book.getAuthor().getLastName()))
		        .andExpect(
		                jsonPath("$.genre.id").value(book.getGenre().getId()))
		        .andExpect(jsonPath("$.genre.name")
		                .value(book.getGenre().getName()))
		        .andExpect(jsonPath("$.comments", hasSize(0)));
	}

	@Test
	public void create() throws Exception {
		BookDto newBook = new BookDto(Integer.MAX_VALUE, "New book",
		        AuthorDto.toDto(author), GenreDto.toDto(genre),
		        new ArrayList<>());
		String bookJson = new ObjectMapper().writeValueAsString(newBook);
		mvc.perform(post(baseUrl).contentType(MediaType.APPLICATION_JSON)
		        .content(bookJson)).andExpect(status().isCreated());
	}

	@Test
	public void update() throws Exception {
		BookDto newBook = new BookDto(1, "update book", AuthorDto.toDto(author),
		        GenreDto.toDto(genre), new ArrayList<>());
		String bookJson = new ObjectMapper().writeValueAsString(newBook);

		mvc.perform(put(baseUrl + "/{id}", book.getId())
		        .contentType(MediaType.APPLICATION_JSON).content(bookJson))
		        .andExpect(status().isOk());
	}

	@Test
	public void _delete() throws Exception {
		mvc.perform(delete(baseUrl + "/{id}", book.getId()))
		        .andExpect(status().isOk());
	}
}
