package com.otus.edu.library.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.edu.library.domain.Author;

public interface AuthorRepository
        extends JpaRepository<Author, Integer>, NamedObjectRepository<Author> {
	Collection<Author> findByFirstNameOrLastName(String firstName,
	        String lastName);

	@Override
	default Collection<Author> findByName(String name) {
		return findByFirstNameOrLastName(name, name);
	}
}
