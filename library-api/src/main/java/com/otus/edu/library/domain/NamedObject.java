package com.otus.edu.library.domain;

public interface NamedObject<T> extends HasId<T> {
	String getName();
}
