package com.otus.edu.library.repository;

import java.util.Collection;

public interface NamedObjectRepository<T> {
	Collection<T> findByName(String name);
}
