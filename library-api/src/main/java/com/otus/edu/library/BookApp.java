package com.otus.edu.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class BookApp {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BookApp.class, args);
	}
}
