package com.otus.edu.library.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.otus.edu.library.domain.Book;
import com.otus.edu.library.dto.BookDto;
import com.otus.edu.library.exception.NotFoundException;
import com.otus.edu.library.repository.BookRepository;
import com.otus.edu.library.service.BookService;

@RestController
@RequestMapping(BookController.BASE_URL)
public class BookController {
	public static final String BASE_URL = "/books";

	private final BookRepository repository;
	private final BookService bookService;

	@Autowired
	public BookController(BookRepository repository, BookService bookService) {
		this.repository = repository;
		this.bookService = bookService;
	}

	@GetMapping("")
	public List<BookDto> findAll() {
		return repository.findAll().stream().map(BookDto::toDto)
		        .collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<BookDto> findOne(@PathVariable("id") int id) {
		return repository.findById(id).map(BookDto::toDto)
		        .map(g -> ResponseEntity.ok(g))
		        .orElse(ResponseEntity.notFound().build());
	}

	@PostMapping()
	public ResponseEntity<BookDto> create(@RequestBody BookDto book) {
		try {
			Book createBook = bookService.createBook(book.getAuthor().getId(),
			        book.getGenre().getId(), book.getName());
			return ResponseEntity
			        .created(URI.create(BASE_URL + "/" + createBook.getId()))
			        .build();
		} catch (NotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<BookDto> update(@PathVariable("id") int id,
	        @RequestBody BookDto book) {
		try {
			return repository.findById(id)
			        .map(b -> bookService.updateBook(b.getId(),
			                book.getAuthor().getId(), book.getGenre().getId(),
			                book.getName()))
			        .map(b -> ResponseEntity.ok().<BookDto>build())
			        .orElse(ResponseEntity.notFound().build());
		} catch (NotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("id") int id) {
		repository.deleteById(id);
	}
}
