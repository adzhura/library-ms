package com.otus.edu.library.domain;

public interface HasId<T> {
	T getId();

	void setId(T id);
}
