package com.otus.edu.library.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.otus.edu.library.domain.Author;
import com.otus.edu.library.dto.AuthorDto;
import com.otus.edu.library.repository.AuthorRepository;

@RestController
@RequestMapping(AuthorController.BASE_URL)
public class AuthorController {
	public static final String BASE_URL = "/authors";

	private final AuthorRepository repository;

	@Autowired
	public AuthorController(AuthorRepository repository) {
		this.repository = repository;
	}

	@GetMapping("")
	public List<AuthorDto> findAll() {
		return repository.findAll().stream().map(AuthorDto::toDto)
		        .collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<AuthorDto> findOne(@PathVariable("id") int id) {
		return repository.findById(id).map(AuthorDto::toDto)
		        .map(g -> ResponseEntity.ok(g))
		        .orElse(ResponseEntity.notFound().build());
	}

	@PostMapping("")
	public ResponseEntity<AuthorDto> create(@RequestBody AuthorDto authorDto) {
		return ResponseEntity.created(URI.create(BASE_URL + "/"
		        + repository.save(AuthorDto.toDomain(authorDto)).getId()))
		        .build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<AuthorDto> update(@PathVariable("id") int id,
	        @RequestBody AuthorDto author) {
		return repository.findById(id).map(g -> {
			Author domain = AuthorDto.toDomain(author);
			domain.setId(g.getId());
			return repository.save(domain);
		}).map(g -> ResponseEntity.ok().<AuthorDto>build())
		        .orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("id") int id) {
		repository.deleteById(id);
	}
}
