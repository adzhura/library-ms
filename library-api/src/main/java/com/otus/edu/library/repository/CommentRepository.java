package com.otus.edu.library.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.edu.library.domain.Book;
import com.otus.edu.library.domain.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
	Collection<Comment> findByUserName(String userName);

	Collection<Comment> findByBook(Book book);
}
