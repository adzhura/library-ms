package com.otus.edu.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.edu.library.domain.Book;

public interface BookRepository
        extends JpaRepository<Book, Integer>, NamedObjectRepository<Book> {

}
