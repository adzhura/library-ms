package com.otus.edu.library.dto;

import com.otus.edu.library.domain.Comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto {
	private Integer id;

	private String userName;

	private String text;

	public static CommentDto toDto(Comment comment) {
		return new CommentDto(comment.getId(), comment.getUserName(),
		        comment.getText());
	}
}
