package com.otus.edu.library.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of = { "id", "name" })
@Entity
@ToString(of = { "id", "name", "genre", "author" })
@Table(name = "BOOKS")
public class Book implements NamedObject<Integer> {
	@Id
	@Column(name = "BK_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "TITLE", nullable = false)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY, /* cascade = CascadeType.ALL, */ optional = false)
	@JoinColumn(name = "GNR_GNR_ID", nullable = false)
	private Genre genre;

	@ManyToOne(fetch = FetchType.LAZY, /* cascade = CascadeType.ALL, */ optional = false)
	@JoinColumn(name = "AUT_AUT_ID", nullable = false)
	private Author author;

	@OneToMany(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Comment> comments;

	public Book() {
	}

	public Book(Author author, Genre genre, String name) {
		this.name = name;
		this.genre = genre;
		this.author = author;
	}
}
