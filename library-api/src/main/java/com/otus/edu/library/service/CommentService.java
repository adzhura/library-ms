package com.otus.edu.library.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.otus.edu.library.domain.Book;
import com.otus.edu.library.domain.Comment;
import com.otus.edu.library.exception.NotFoundException;
import com.otus.edu.library.repository.BookRepository;
import com.otus.edu.library.repository.CommentRepository;

@Service
@Transactional
public class CommentService {
	private final BookRepository bookRepository;

	private final CommentRepository commentRepository;

	@Autowired
	public CommentService(BookRepository bookRepository,
	        CommentRepository commentRepository) {
		this.bookRepository = bookRepository;
		this.commentRepository = commentRepository;
	}

	public Comment createComment(int bookId, String userName, String text) {
		Optional<Book> book = bookRepository.findById(bookId);

		if (book.isPresent()) {
			Comment comment = new Comment();
			comment.setUserName(userName);
			comment.setText(text);
			comment.setBook(book.get());
			book.get().getComments().add(comment);

			return commentRepository.save(comment);
		}

		throw new NotFoundException();
	}

	public Collection<Comment> getComments(int bookId) {
		return bookRepository.findById(bookId)
		        .flatMap(b -> Optional.ofNullable(b.getComments()))
		        .orElse(Collections.emptyList()).stream()
		        .collect(Collectors.toList());
	}

	public Collection<Comment> getComments(String userName) {
		return commentRepository.findByUserName(userName);
	}
}
