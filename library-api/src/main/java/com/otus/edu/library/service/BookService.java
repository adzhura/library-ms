package com.otus.edu.library.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.otus.edu.library.domain.Author;
import com.otus.edu.library.domain.Book;
import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.exception.NotFoundException;
import com.otus.edu.library.repository.AuthorRepository;
import com.otus.edu.library.repository.BookRepository;
import com.otus.edu.library.repository.GenreRepository;

@Service
@Transactional
public class BookService {
	private final AuthorRepository authorRepository;
	private final GenreRepository genreRepository;
	private final BookRepository bookRepository;

	@Autowired
	public BookService(AuthorRepository authorRepository,
	        GenreRepository genreRepository, BookRepository bookRepository) {
		this.authorRepository = authorRepository;
		this.genreRepository = genreRepository;
		this.bookRepository = bookRepository;
	}

	public Book createBook(Integer authorId, Integer genreId, String name) {
		Optional<Author> author = authorRepository.findById(authorId);
		Optional<Genre> genre = genreRepository.findById(genreId);

		if (author.isPresent() && genre.isPresent()) {
			return bookRepository
			        .save(new Book(author.get(), genre.get(), name));
		} else {
			throw new NotFoundException();
		}
	}

	public Book updateBook(Integer bookId, Integer authorId, Integer genreId,
	        String name) {
		Optional<Book> book = bookRepository.findById(bookId);
		Optional<Author> author = authorRepository.findById(authorId);
		Optional<Genre> genre = genreRepository.findById(genreId);

		if (book.isPresent() && author.isPresent() && genre.isPresent()) {
			Book b = book.get();

			b.setAuthor(author.get());
			b.setGenre(genre.get());
			b.setName(name);

			return bookRepository.save(b);
		} else {
			throw new NotFoundException();
		}
	}
}
