package com.otus.edu.library.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.otus.edu.library.dto.Page;

@RestController
public class MainController {
	@GetMapping("/")
	public String main() {
		return "index";
	}

	@GetMapping("/pages")
	public Page[] pages() {
		return new Page[] { new Page("Genres", "genres"),
		        new Page("Authors", "authors"), new Page("Books", "books"),
		        new Page("Admin", "admin") };
	}

}
