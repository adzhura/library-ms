package com.otus.edu.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.otus.edu.library.domain.Comment;
import com.otus.edu.library.dto.CommentDto;
import com.otus.edu.library.exception.NotFoundException;
import com.otus.edu.library.service.CommentService;

@RestController
public class CommentController {
	private final CommentService commentService;

	@Autowired
	public CommentController(CommentService commentService) {
		this.commentService = commentService;
	}

	@PostMapping("/books/{bookId}/comments")
	public ResponseEntity<Comment> addComment(
	        @PathVariable("bookId") int bookId,
	        @RequestBody CommentDto comment) {
		if (comment.getText() != null
		        && comment.getText().trim().length() > 0) {
			try {
				commentService.createComment(bookId, comment.getUserName(),
				        comment.getText());
				return ResponseEntity.status(HttpStatus.CREATED).build();
			} catch (NotFoundException e) {
				return ResponseEntity.notFound().build();
			}
		} else {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
	}
}
