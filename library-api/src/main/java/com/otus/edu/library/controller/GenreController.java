package com.otus.edu.library.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.otus.edu.library.domain.Genre;
import com.otus.edu.library.dto.GenreDto;
import com.otus.edu.library.repository.GenreRepository;

@RestController
@RequestMapping(GenreController.BASE_URL)
public class GenreController {
	public static final String BASE_URL = "/genres";

	private final GenreRepository repository;

	@Autowired
	public GenreController(GenreRepository repository) {
		this.repository = repository;
	}

	@GetMapping("")
	public List<GenreDto> findAll() {
		return repository.findAll().stream().map(GenreDto::toDto)
		        .collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<GenreDto> findOne(@PathVariable("id") int id) {
		return repository.findById(id).map(GenreDto::toDto)
		        .map(g -> ResponseEntity.ok(g))
		        .orElse(ResponseEntity.notFound().build());
	}

	@PostMapping("")
	public ResponseEntity<GenreDto> create(@RequestBody GenreDto genreDto) {
		return ResponseEntity
		        .created(URI.create(BASE_URL + "/"
		                + repository.save(GenreDto.toDomain(genreDto)).getId()))
		        .build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<GenreDto> update(@PathVariable("id") int id,
	        @RequestBody GenreDto genre) {
		return repository.findById(id).map(g -> {
			Genre domain = GenreDto.toDomain(genre);
			domain.setId(g.getId());
			return repository.save(domain);
		}).map(g -> ResponseEntity.ok().<GenreDto>build())
		        .orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("id") int id) {
		repository.deleteById(id);
	}
}
