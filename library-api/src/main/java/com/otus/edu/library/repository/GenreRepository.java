package com.otus.edu.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.edu.library.domain.Genre;

public interface GenreRepository
        extends JpaRepository<Genre, Integer>, NamedObjectRepository<Genre> {

}
