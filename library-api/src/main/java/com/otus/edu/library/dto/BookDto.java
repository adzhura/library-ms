package com.otus.edu.library.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.otus.edu.library.domain.Book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {
	private Integer id;

	private String name;

	private AuthorDto author;

	private GenreDto genre;

	private List<CommentDto> comments;

	public static BookDto toDto(Book book) {
		return new BookDto(book.getId(), book.getName(),
		        AuthorDto.toDto(book.getAuthor()),
		        GenreDto.toDto(book.getGenre()),
		        Optional.ofNullable(book.getComments())
		                .map(comments -> comments.stream()
		                        .map(CommentDto::toDto)
		                        .collect(Collectors.toList()))
		                .orElse(new ArrayList<>()));
	}
}
