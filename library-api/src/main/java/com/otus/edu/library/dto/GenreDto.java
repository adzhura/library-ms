package com.otus.edu.library.dto;

import com.otus.edu.library.domain.Genre;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenreDto {
	private Integer id;

	private String name;

	public static GenreDto toDto(Genre genre) {
		return new GenreDto(genre.getId(), genre.getName());
	}

	public static Genre toDomain(GenreDto genreDto) {
		Genre genre = new Genre(genreDto.getName());

		genre.setId(genreDto.getId());

		return genre;
	}
}
