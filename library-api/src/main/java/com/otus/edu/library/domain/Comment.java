package com.otus.edu.library.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(of = { "id", "userName", "text" })
@EqualsAndHashCode(of = { "id", "userName", "text" })
@Entity
@Table(name = "COMMENTS")
public class Comment implements HasId<Integer> {
	@Id
	@Column(name = "CMT_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "USER_NAME", nullable = false)
	private String userName;

	@Column(name = "TEXT", nullable = false)
	private String text;

	@ManyToOne(fetch = FetchType.LAZY, /* cascade = CascadeType.ALL, */optional = false)
	@JoinColumn(name = "BK_BK_ID", nullable = false)
	private Book book;

	public Comment() {
		super();
	}

	public Comment(Book book, String userName, String text) {
		this.book = book;
		this.userName = userName;
		this.text = text;
	}
}
