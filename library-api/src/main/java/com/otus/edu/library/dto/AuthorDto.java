package com.otus.edu.library.dto;

import com.otus.edu.library.domain.Author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDto {
	private Integer id;

	private String firstName;
	private String lastName;

	public static AuthorDto toDto(Author author) {
		return new AuthorDto(author.getId(), author.getFirstName(),
		        author.getLastName());
	}

	public static Author toDomain(AuthorDto authorDto) {
		Author author = new Author(authorDto.getFirstName(),
		        authorDto.getLastName());

		author.setId(authorDto.getId());

		return author;
	}
}
